# Guild Wars 2 Capstone Project
------------------------------

The Guild Wars 2 Capstone Project aims to create additional tools that will augment the capabilities of the commander role without violating the EULA created by the ArenaNet.

## Section Breakdown
-------------------

1. Background
2. Installation Instructions

## 1. Background
---------------



## 2. Installation Instructions
------------------------------

This section contains the steps taken to setup the web application onto a web server.

### Setting Up Permissions
--------------------------

Using the command line, navigate to the root of the project directory. After navigating to the root directory, enter the following command:

    chown -R www-data app/tmp

### Create the Database
-----------------------

Next step is to create the database for the web application. If possible, it is suggested to start with an empty database for this web application.

Before setting up the database, navigate to the web application site using a web browser. Once you are on the site, follow the instructions on the page.
