# Map
------------------------------

A map of a WvWvW region that displays objectives and match stats and allows the user to sketch freehand. Implemented using Leaflet Maps API. 

## Section Breakdown
-------------------

1. Usage
	1. Unregistered User
	2. Squad Member
	3. Commander
2. Server Side
	1. Controller
	2. Models
	3. View
3. Client Side
	1. JavaScript
	2. CSS
4. Issues
5. User FAQs

## 1. Usage
----------------

### 1. Unregistered User
----------------------------

Unregistered users can navigate to the Map page but will not see any match or objective statistics and will not be able to sketch on the map.

### 2. Squad Member
-------------------------

Squad members (users registered as squad members under one specified commander for a specified match) will see the following on the Map page:

* Scores for each army
* PPT for each army
* Red, Green and Blue bloodlust (indicated by icons next to PPT)
* Objective markers, with icon type corresponding to the objective type and icon colour corresponding to objective owner; a shield will appear on the marker if the objective is owned by a guild 
* RI remaining for objectives (displayed next to the objective marker)
* Objective names (appear when user hovers over the objective marker)
* Popup displays when user clicks on an objective marker; shows:
	* objective name
	* time since last capture
	* guild name and image, if applicable
	
Squad members will also see all sketches the commander has shared with the squad. Squad members can toggle the visibility of individual sketches on their personal map using the layer control. 

### 3. Commander
----------------

Commanders (users registered as commanders) will see everything squad members see (above). In addition, commanders will see ALL the sketches they have created, not just the ones they have shared with a squad. Commanders can access the following functionality not available to squad members:

* Double-clicking on the map puts it into sketch mode. While in sketch mode, the commander can scribble freehand on the map. A sketch control appears on the map which allows the commander to change the size and color of his sketching tool, or change the tool to an eraser (and back). The map cannot be panned or zoomed while in sketch mode.
*  Double-clicking on the map while in sketch mode exits sketch mode and uploads the new sketch to the app server. If the commander specified his squad when logging in, the new sketch will automatically be shared with all his squad members; otherwise, it remains private. 
* Pressing ESC while in sketch mode exits sketch mode without uploading the new sketch to the app server (the sketch is lost forever).
* Pressing the save icon opens the save dialog, which allows the commander to save all currently visible sketches as a single sketch under a new name. If the commander specified his squad when logging in, the newly saved sketch will automatically be shared with all his squad members; otherwise, it remains private.
* The commander can use the layer control to toggle the visibility of individual sketches on their personal map, or to permanently delete sketches from all maps and the app server. If the commander specified his squad when logging in, sketches visible to his squad will appear in bold font in the layer control and private sketches will appear in regular font. Deleting a sketch permanently removes it from all maps, even if the commander did not specify his squad when logging in. 

## 2. Server Side
----------------

### 1. Controller
----------------
All map-specific server-side functionality is contained in app/Controller/MapsController.php . Important functions are:

* Uploading new sketches to the app server/database  

* Fetching map and objective stat jsons from gw2stats.net

* Fetching sketches from the app server/database

* Updating squad member views when the commander uploads or deletes a sketch

* maintaining the app database tables and server image storage

### 2. Models
----------------

There are 5 map-specific database tables (images, image_groups, squads, squad_members, and squad_sketches) and 5 corresponding models (Image, ImageGroup, Squad, SquadMember, and SquadSketch). In addition, the map makes use of the users table and User model to manage user sessions and security. 

### 3. View
----------------

The map has one view, map.ctp 


## 3. Client Side
----------------
### 1. JavaScript 
------------------

#### Blob.js, canvas-toBlob.js, FileSaver.js

Contain unmodified code from http://purl.eligrey.com/github/ which downloads a canvas sketch to the user's Downloads folder as a .png. Now replaced by code which downloads a canvas sketch to a remote database. May be useful if app is to be extended to allow users to download their sketches. 

#### leaflet_canvas_layer.js, label-src.js, bootstrap.js, jquery.js, bootstrap.min.js, jquery.min.js 

Unmodified frameworks and plugins

#### mysketch.js

sketch.js (http://intridea.github.io/sketch.js/) modified in the following ways: 
     * returns control to the leaflet map functions when not in sketch mode 
     * clears canvas when clearCanvas flag is set
     * automatically switches to the marker tool when marker color is changed
     
#### sketch_control.js 

Implements a custom leaflet control used in sketch mode. 

#### save_control.js

Implements a custom leaflet control used to open the save dialog

#### doc_event_response.js

Responds to user pressing ESC and ENTER

#### objective_markers.js

Defines marker classes; contains functions for creating and updating objective markers based on data collected from jsons

#### match_stats.js

Contains function for updating scores/PPT/bloodlust based on data collected from jsons

#### map_session_manager.js

Contains functions for:

* beginning a map session (getting sketches from app server/database and adding them to the map and layer control)
* (non-commanders only) setting a timeout to a function that updates the map session (adds sketches that the commander shared with the squad since the last update; deletes sketches that the commander deleted since the last update)

#### leaflet_sketch_layer.js

A custom leaflet layer that extends the CanvasLayer plugin; creates a sketch canvas on top of the map.

#### overlay_manager.js

Contains functions for:

* sending ajax calls to MapsController.php to 
	* upload a new sketch to the app server/database
	* delete a sketch from the app server/database
	* save a group of one or more existing sketches (in the app database) as one sketch under a new name 
* locally:
	* remove a sketch from the map / layer control
	* add a sketch to the map / layer control
	* combine one or more existing sketches (on the layer control) into one sketch under a new name	
	
#### get_jsons.js

Sets timeouts to functions that update the match scores/PPT/bloodlust and the objective owner / guild (if applicable) / ri remaining (if applicable). Calls functions in MapsController.php which use gw2stats.net to fetch the necessary jsons (can't call gw2stats.net api directly from javascript due to cross-origin restrictions on the gw2stats.net server).  

#### myleaflet.js

Contains functions that:

* Create the map tileLayer and sketchLayer 
* Transition to/from the save dialog (implemented as a modal window)
* Enter/exit sketch mode when the user (commanders only) doubleclicks on the map

## 4. Issues
--------------------------

### mysketch.js
--------------------------------------
* exit() used in Sketch.prototype.onEvent to return to map; gives error 'exit is not defined'
* exit() randomly called during brush size change? (probably due to doubleclicking)
 
### MapsController.php
-------------------------
* (in getUniqueName) needs better name filtering: don't allow control characters or leading/trailing/consecutive whitespace  




## 5. User FAQs
--------------------------  

* Where is the layer control?
The layer control appears in the top right corner of the map if and only if the map has at least one sketch on it. 
* Where is the save control (commanders only)?
The save control appears in the top right corner of the map if and only if the map has at least one sketch on it. 
* As a commander, how do I delete a sketch?
Hover over the layer control, find the name of the sketch you want to delete, and press the 'X' to the right of the sketch name. WARNING: Deleted sketches are permanently removed from the server and will disappear from all maps (yours and your squad members') regardless of whether you signed in to your squad at login. There is currently no functionality for recovering a deleted sketch. 
* As a commander, how do I share an existing sketch with my squad?
Save while you are signed in to your squad and the sketch is visible on your map.
* As a commander, how do I un-share an existing sketch with my squad?
There is currently no functionality for 'un-sharing' sketches without deleting them. 
