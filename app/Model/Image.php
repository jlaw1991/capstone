<?php
App::uses('AppModel', 'Model');
class Image extends AppModel {
	public $name = 'Image'; 

	public $belongsTo = array(
		'ImageGroup' => array(
			'className' => 'ImageGroup'
		)
	);
}
?>