<?php
App::uses('AppModel', 'Model');
class SquadSketch extends AppModel {
	public $name = 'SquadSketch';
	
	public $belongsTo = array(
		'Squad' => array(
			'className' => 'Squad',
			'foreignKey' => 'squad_id'
		),
		'ImageGroup' => array(
			'className' => 'ImageGroup',
			'foreignKey' => 'image_group_id'
		)
	);
}
?>