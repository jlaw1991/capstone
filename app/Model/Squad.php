<?php
App::uses('AppModel', 'Model');
class Squad extends AppModel {
	public $name = 'Squad';

	public $hasMany = array(
		'SquadMember' => array(
			'className' => 'SquadMember',
			'foreignKey' => 'squad_id',
			'dependent' => true,
			'exclusive' => false
		),
		'SquadSketch' => array(
			'className' => 'SquadSketch',
			'foreignKey' => 'squad_id',
			'dependent' => true,
			'exclusive' => false
		),
		'Event' => array(
			'className' => 'FullCalendar.Event',
			'foreignKey' => 'id',
			'dependent' => false
		)
	);

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

}
?>