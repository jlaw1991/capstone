<?php
App::uses('AppModel', 'Model');
class SquadMember extends AppModel {
	public $name = 'SquadMember';

	public $belongsTo = array(
		'Squad' => array(
			'className' => 'Squad',
			'foreignKey' => 'squad_id'
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);
}
?>