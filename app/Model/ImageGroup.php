<?php
App::uses('AppModel', 'Model');
class ImageGroup extends AppModel {
	public $name = 'ImageGroup';
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

	public $hasMany = array(
		'Image' => array(
			'className' => 'Image',
			'foreignKey' => 'image_group_id',
			'dependent' => true,
			'exclusive' => false
		),
		'SquadSketch' => array(
			'className' => 'SquadSketch',
			'foreignKey' => 'image_group_id',
			'dependent' => true,
			'exclusive' => false
		)
	);

}
?>