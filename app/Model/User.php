<?php

App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
	
	public $name = 'User';

	public $hasMany = array(
		'ImageGroup' => array(
			'className' => 'ImageGroup',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'exclusive' => false
		),
		'User' => array(
			'className' => 'SquadMember',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'exclusive' => false
		),
		'Squad' => array(
			'className' => 'Squad',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'exclusive' => false
		),
		'Event' => array(
			'className' => 'FullCalendar.Event',
			'foreignKey' => 'assignee_id',
			'dependent' => false
		)
	);

	public $validate = array(
		'username' => array(
			'usernameRule-1' => array(
				'rule' => 'notEmpty',
				'message' => 'A username is required'
			),
			'usernameRule-2' => array(
				'rule' => 'isUnique',
				'message' => 'This username has already been taken'
			)
		),
		'password' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A password is required',
			),
		),
		'role' => array(
			'valid' => array(
				'rule' => array('inList', array('commander', 'player')),
				'message' => 'Please enter a valid role',
				'allowEmpty' => false,
			),
		)
	);

	public function beforeSave($options = array()) {
		if(isset($this->data[$this->alias]['password'])) {
			$passwordHasher = new SimplePasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash(
				$this->data[$this->alias]['password']
			);
		}
		return true;
    }
}
