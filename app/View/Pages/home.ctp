<!-- /app/View/Pages/home.ctp -->

<div class="jumbotron">
    <div class="container">
        <h1><?php echo 'Guild Wars 2 WvWvW Tools'; ?></h1>
        <p>The Guild Wars 2 World vs World vs World tools provides a strategy 
        interface for commanders and players alike. The aim of this web 
        application is to provide more strategy for the World vs World vs World 
        events and hopefully improve the gameplay.</p>
    </div>
</div>

<div class="row">
    <div class="container">
        <div class="row">
            <div class="page-header"><h2>Map</h2></div>
            <p><?php echo $this->Html->image('map.png', array('alt' => 'Map', 'class' 
            => 'img-rounded col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1')); ?></p>
        </div>
        <div class="row">
            <p class="col-lg-6 col-lg-offset-3">The map provides commanders with a strategy interface that allows 
            path drawing.</p>
        </div>
        <div class="row">
            <div class="page-header"><h2>Calendar Component</h2></div>
            <p><?php echo $this->Html->image('calendar.png', array('alt' => 
                'Calendar', 'class' => 'img-rounded col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 col-xl-10 col-xl-offset-1 col-lg-10 col-lg-offset-1')); ?></p> 
        </div>
        <div class="row">
            <p class="col-lg-6 col-lg-offset-3">The calendar provides commanders with a scheduling tool and a 
            squad management tool</p>
        </div>
    </div>
</div>

