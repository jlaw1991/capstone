<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
    </title>
    <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('sticky-footer');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->Html->script('http://code.jquery.com/jquery-latest.js');
        echo $this->Html->script('bootstrap');
    ?>
</head>
<body>

    <div id="container">
        <div id="wrap">
            <div id="header">
                <div class="navbar navbar-default navbar-static-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <?php echo $this->Html->link(
                                'Guild Wars 2 WvWvW Tool',
                                array('plugin' => '', 'controller' => 'pages', 'action' => 'display'),
                                array('class' => 'navbar-brand')
                                ); ?>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><?php echo $this->Html->link('Home', array(
                                        'plugin' => '',
                                        'controller' => 'pages', 
                                        'action' => 'display')
                                    ); ?>
                                </li>
                                <li><?php echo $this->Html->link('Map', array(
                                        'plugin' => '',
                                        'controller' => 'maps', 
                                        'action' => 'map')
                                    ); ?>
                                </li>
                                <li><?php echo $this->Html->link('Calendar', '/full_calendar'); ?></li>
                                <?php if($loggedIn == false) { ?>
                                <li><?php echo $this->Html->link('Login', array(
                                        'plugin' => '',
                                        'controller' => 'users',
                                        'action' => 'login')
                                    );?>
                                </li>
                                <?php } else { ?>
                                <li><?php echo $this->Html->link('Logout', 
                                array(
                                        'plugin' => '',
                                        'controller' => 'users',
                                        'action' => 'logout')
                                    );?>
                                </li>
                                <?php }?>
                            </ul>
                            <?php if($loggedIn == true) { ?>
                            <ul class="nav navbar-nav navbar-right">
                                <li><p class="navbar-text">Hello, <?php echo $loginName;?></p></li>
                            </ul>
                            <?php } ?>
                        </div><!--/.navbar-collapse -->
                    </div>
                </div>
            </div>

            <div id="content">
                <div class="container">
                    <?php echo $this->Session->flash(); ?>

                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>

        <div id="footer">
            <div class="container">
                <p class="text-muted">Footer content goes here</p>
            </div>
        </div>
    </div>

    <?php echo $this->Html->script('http://code.jquery.com/jquery-latest.js'); ?>
    <?php echo $this->Html->script('bootstrap'); ?>
    
    <?php //echo $this->element('sql_dump'); ?>
</body>
</html>
