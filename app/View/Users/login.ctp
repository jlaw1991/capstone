<div class="users form">
<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User', array('class' => 'form-signin')); ?>
	<fieldset>
		<legend>
			<?php echo __('Please enter your username and password'); ?>
		</legend>
		<?php echo $this->Form->input('username', array(
                'label' => 'Username',
                'class' => 'form-control')
            );
        echo $this->Form->input('password', array('class' => 'form-control'));
		?>
		<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-primary')); ?>
	</fieldset>
	<?php echo $this->Form->end(); ?>
	<?php echo $this->Html->link('New user?', array(
		'controller' => 'users',
		'action' => 'add')
		);
	?>
</div>
