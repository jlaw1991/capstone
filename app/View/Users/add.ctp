<div class="users form">
    <?php echo $this->Form->create('User', array('class' => 'form-signin')); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <?php echo $this->Form->input('username', array(
                'label' => 'Username',
                'class' => 'form-control')
            );
        echo $this->Form->input('password', array('class' => 'form-control'));
        echo $this->Form->input('role', array(
            'options' => array('commander' => 'commander', 'player' => 'player'),
            'class' => 'form-control'
            ));
        ?>
        <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-primary')); ?>
    </fieldset>
    <?php echo $this->Form->end(); ?>
</div>