<!-- /app/View/Maps/map.ctp -->

<h1><?php echo 'WvWvW Map'; ?></h1>

<!--<div class="container-fluid">
<?php echo $this->Form->create('Map'); ?>
	<fieldset>
		<div class="row"><legend><?php echo __("Match Switcher"); ?></legend></div>
		<div class="row"><div class="col-lg-12"><?php echo $this->Form->select('squad_id', $squads, array('class' => 'form-control'));?></div></div>
		<div class="row"><div class="col-lg-4"><?php echo $this->Form->submit('Submit', array('class' => 'btn btn-primary')); ?></div></div>
	</fieldset>
<?php echo $this->Form->end(); ?>
</div>-->

<div id="scratch" style="display: none"></div>
<div id="match_details">
<!-- Match Title -->
<div class="armies match_title">
<span id="Red_name" class="red_text">  Red</span>
<span class="vs">&nbsp vs &nbsp </span>
<span id="Green_name" class="green_text">  Green</span> 
<span class="vs">&nbsp vs &nbsp </span>
<span id="Blue_name" class="blue_text"> Blue</span>
</div>

<!-- Red stats bar -->
<div id="Red_stats" class="red_text armies">

<div class="stats_bar" style="width:25%;">
<span id="Red_score_bar" class="red_bar stats_bar"></span>
</div>

<div class="stats_bar" id="Red_score" style="width:10%;"></div>

<div class="stats_bar" style="width:25%;">
<span id="Red_ppt_bar" class="red_bar stats_bar"></span>
</div>

<div class="stats_bar" id="Red_ppt" style="width:10%;"></div>
</div>

<!-- Green stats bar -->
<div id="Green_stats" class="green_text armies">

<div class="stats_bar" style="width:25%;">
<span id="Green_score_bar" class="green_bar stats_bar"></span>
</div>

<div class="stats_bar" id="Green_score" style="width:10%;"></div>

<div class="stats_bar" style="width:25%;">
<span id="Green_ppt_bar" class="green_bar stats_bar"></span>
</div>

<div class="stats_bar" id="Green_ppt" style="width:10%;"></div>



</div>


<!-- Blue stats bar -->
<div id="Blue_stats" class="blue_text armies">

<div class="stats_bar" style="width:25%;">
<span id="Blue_score_bar" class="blue_bar stats_bar"></span>
</div>

<div class="stats_bar" id="Blue_score" style="width:10%;"></div>

<div class="stats_bar" style="width:25%;">
<span id="Blue_ppt_bar" class="blue_bar stats_bar"></span>
</div>

<div class="stats_bar" id="Blue_ppt" style="width:10%;"></div>


</div>
</div>
<div style="padding:9px;"></div>

<div id="squad_msg"> </div>
<div id="json_error_msgs"></div>
<div style="padding:9px;"></div>

<div id="map-container">
<div id="map"></div>


<div id="mask" onclick="exitModal()"></div>
<div id="popup">
	<div class="save-heading">
		Save
	</div>
	<hr />
	<div>
		All sketches visible on the map will be saved as one sketch layer.
	</div>
	<br />
	Save as: &nbsp; &nbsp; <input type="text" id="save-as" value="" maxlength="100">
	<div id="save-buttons">
	<input type="button" id="save-button" class="mybutton savebutton" value="Save" onclick="saveAsLayerGroup()">  &nbsp; &nbsp;  &nbsp; &nbsp;
	<input type="button" id="cancel-button" class="mybutton" value="Cancel" onclick="exitModal()">
	</div>
</div>
</div>
<!--<pre>
	<?php echo "sm_arr: ";print_r($sm_arr); ?>
	<?php echo "squads: ";print_r($squads); ?>
</pre>-->


<!--<pre>
<?php //echo $this->element('sql_dump'); ?>
</pre>-->


<script type="text/javascript">
	// Folders
	var saveIconFolder = '<?php echo $this->Html->url("/", true); ?>app_icons/';
	var iconFolder =  '<?php echo $this->Html->url("/", true); ?>wvw_imgs/';
	var bloodlustFolder ='<?php echo $this->Html->url("/", true); ?>bloodlust/bloodlust_';
	
	// Sketch directory
	var imgDir = '<?php echo $this->Html->url("/", true); ?>sketches/'; 
	
	// Functions
	var begin_map_session_url = '/maps/beginMapSession';
	var update_map_session_url = '/maps/updateMapSession';
	var get_images_url = '/maps/getImages';
	var get_server_and_match_url = '/maps/getServerAndMatch';
	var get_image_file_url = '/maps/getImageFile/';//<?php echo $this->Html->url(array('controller' => 'MapsController', 'action' => 'getImageFile', 22)); ?>;
	
	var get_match_stats_url = '/maps/getMatchStats';
	var get_obj_stats_url = '/maps/getObjStats';
	var delete_image_group_url = '/maps/deleteImageGroup';
	var get_image_file_url = '/maps/getImageFile';
	var get_unique_name_url = '/maps/getUniqueName';
	var merge_image_groups_url = '/maps/mergeImageGroups';
	var upload_base_64_url = '/maps/uploadBase64';//<?php echo Router::url(array('controller'=>'maps','action'=>'uploadBase64')) ?>;
</script>

<?php 
	echo $this->Html->css('http://cdn.leafletjs.com/leaflet-0.7/leaflet.css');
    echo $this->Html->css('map.css');
    echo $this->Html->css('label.css');
	echo $this->Html->script('http://cdn.leafletjs.com/leaflet-0.7/leaflet.js');
    echo $this->Html->script('leaflet_canvas_layer.js');
    echo $this->Html->script('leaflet_sketch_layer.js');
    echo $this->Html->script('sketch_control.js');
    echo $this->Html->script('doc_event_response.js');
    echo $this->Html->script('save_control.js');
    echo $this->Html->script('overlay_manager.js');
    echo $this->Html->script('map_session_manager.js');
    echo $this->Html->script('FileSaver.js');
    echo $this->Html->script('Blob.js');
    echo $this->Html->script('canvas-toBlob.js');
    echo $this->Html->script('get_jsons.js');
    echo $this->Html->script('label-src.js');
    echo $this->Html->script('match_stats.js');
    echo $this->Html->script('objective_markers.js');
    echo $this->Html->script('mysketch.js');
    echo $this->Html->script('myleaflet.js'); 
?>
