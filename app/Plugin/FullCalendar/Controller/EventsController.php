<?php
/*
 * Controller/EventsController.php
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */

class EventsController extends FullCalendarAppController {

	var $name = 'Events';
        var $paginate = array(
            'limit' => 15
        );

        function index() {
		$this->Event->recursive = 1;
		$this->set('events', $this->paginate());
	}

	function view($id = null) {
		$squad = ClassRegistry::init('Squad');
		$squad_member = ClassRegistry::init('SquadMember');
		$user = ClassRegistry::init('User');

		if (!$id) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action' => 'index'));
		}

		$squad_arr = $squad->find('first', array('fields' => 'id', 'conditions' => array('event_id' => $id)));
		$sm_arr = $squad_member->find('list', array('fields' => 'user_id', 'conditions' => array('squad_id' => $squad_arr['Squad']['id'])));

		if (!empty($this->data)) {

			CakeLog::write("jsonlog", json_encode($this->request->data));

			if (array_key_exists('User', $this->request->data)) {
				$squad_member->deleteAll(array('SquadMember.user_id' => $this->request->data['User']['id'], 'squad_id' => $squad_arr['Squad']['id']));
				$this->Session->setFlash(__("User has been removed."));
				$this->redirect(array('action' => 'index'));
			}

			if (!in_array($this->Auth->user('id'), $sm_arr)) {
				$squad_member->create();
				$sm_data = array('user_id' => $this->Auth->user('id'), 'squad_id' => $squad_arr['Squad']['id']);
				$squad_member->set($sm_data);
				if ($squad_member->save()) {
					$this->Session->setFlash(__('You have joined the match', true));
					$this->redirect(array('action' => 'index'));
				}
			} else {
				$squad_member->deleteAll(array('SquadMember.user_id' => $this->Auth->user('id'), 'SquadMember.squad_id' => $squad_arr['Squad']['id']), false);
				$this->Session->setFlash(__('You have left the match', true));
				$this->redirect(array('action' => 'index'));
			}
		}

		$this->set('event', $this->Event->read(null, $id));
		$this->set('current_user', $this->Auth->user('id'));
		$this->set('squad_members', $sm_arr);
		$this->set('user_list', $user->find('list', array('fields' => array('id', 'username'), 'conditions' => array('id' => $sm_arr))));
	}

	function add() {
		$user = ClassRegistry::init('User');
		$squad = ClassRegistry::init('Squad');
		$squad_member = ClassRegistry::init('SquadMember');

		if (!empty($this->data)) {
			$this->Event->create();
			$this->request->data['Event']['assignee_id'] = $this->Auth->user('id');
			$match_string = $this->request->data['Event']['title'];
			if ($this->Event->save($this->data)) {
				
				$squad->create();
				$s_data = array('match_string' => $match_string, 'user_id' => $this->Auth->user('id'), 'event_id' => $this->Event->getLastInsertId());
				$squad->set($s_data);
				$squad->save();

				$squad_member->create();
				$sm_data = array('user_id' => $this->Auth->user('id'), 'squad_id' => $squad->getLastInsertId());
				$squad_member->set($sm_data);
				$squad_member->save();

				$this->Session->setFlash(__('The event has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.', true));
			}
		}
		$this->set('eventTypes', $this->Event->EventType->find('list'));
		$this->set('events', $this->Event->find('all'));

	}

	function edit($id = null) {
		$user = ClassRegistry::init('User');
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->request->data['Event']['assignee_id'] = $this->Auth->user('id');
			if ($this->Event->save($this->data)) {
				$this->Session->setFlash(__('The event has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Event->read(null, $id);
		}
		$this->set('eventTypes', $this->Event->EventType->find('list'));
		
		//$this->set('userlist', $user->find('list', array('fields' => 'username')));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for event', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Event->delete($id)) {
			$this->Session->setFlash(__('Event deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Event was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

        // The feed action is called from "webroot/js/ready.js" to get the list of events (JSON)
	function feed($id=null) {
		$this->layout = "ajax";
		$vars = $this->params['url'];
		$conditions = array('conditions' => array('UNIX_TIMESTAMP(start) >=' => $vars['start'], 'UNIX_TIMESTAMP(start) <=' => $vars['end']));
		$events = $this->Event->find('all', $conditions);
		foreach($events as $event) {
			if($event['Event']['all_day'] == 1) {
				$allday = true;
				$end = $event['Event']['start'];
			} else {
				$allday = false;
				$end = $event['Event']['end'];
			}
			$data[] = array(
					'id' => $event['Event']['id'],
					'assignee' => $event['Event']['assignee_id'],
					'title'=>$event['Event']['title'],
					'start'=>$event['Event']['start'],
					'end' => $end,
					'allDay' => $allday,
					'url' => Router::url('/') . 'full_calendar/events/view/'.$event['Event']['id'],
					'details' => $event['Event']['details'],
					'className' => $event['EventType']['color']
			);
		}
		$this->set("json", json_encode($data));
	}

        // The update action is called from "webroot/js/ready.js" to update date/time when an event is dragged or resized
	function update() {
		$vars = $this->params['url'];
		$this->Event->id = $vars['id'];
		$this->Event->saveField('start', $vars['start']);
		$this->Event->saveField('end', $vars['end']);
		$this->Event->saveField('all_day', $vars['allday']);
	}
	
	function addNewUser() {

	}

}
?>
