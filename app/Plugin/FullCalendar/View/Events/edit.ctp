<?php
/*
 * View/Events/edit.ctp
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */
?>
<div class="events form container">
	<div class="row">
<?php echo $this->Form->create('Event');?>
		<fieldset>
	 		<div class="row"><legend><?php echo __('Edit Match'); ?></legend></div>
			<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('id'); ?></div></div>
			<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('event_type_id', array('class' => 'form-control')); ?></div></div>
			<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('title', array('class' => 'form-control')); ?></div></div>
			<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('details', array('class' => 'form-control')); ?></div></div>
			<div class="row">
				<div class="col-xs-6 col-lg-6"><?php echo $this->Form->input('start'); ?></div>
				<div class="col-xs-6 col-lg-6"><?php echo $this->Form->input('end'); ?></div>
			</div>
			<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('all_day'); ?></div></div>
			<div class="row"><div class="col-lg-12"><?php echo $this->Form->submit('Submit', array('class' => 'btn btn-primary')); ?></div></div>
		</fieldset>
		<?php echo $this->Form->end();?>
	</div>
</div>
<div class="btn-group">
	<?php echo $this->Html->link(__('View Match', true), array('plugin' => 'full_calendar', 'action' => 'view', $this->Form->value('Event.id')), array('class' => 'btn btn-default')); ?>
	<?php echo $this->Html->link(__('Manage Matches', true), array('plugin' => 'full_calendar', 'action' => 'index'), array('class' => 'btn btn-default'));?>
	<?php echo $this->Html->link(__('View Calendar', true), array('plugin' => 'full_calendar', 'controller' => 'full_calendar'), array('class' => 'btn btn-default')); ?>
</div>
