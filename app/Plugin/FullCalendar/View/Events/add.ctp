
<?php
/*
 * View/Events/add.ctp
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */
?>

<html>
<style>
	#table-wrapper{
		 position:relative;
	}
	#table-scroll {
		overflow:auto;
		height:200px;
	}
	h1 {
		text-indent:2%;
		font-weight:bold;
	}
	
</style>
<div class="events form container">

<?php echo $this->Form->create('Event');?>
	<fieldset>
 		<div class="row"><div class="col-lg-12"><legend><?php echo __('Add Match'); ?></legend></div></div>
		<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('event_type_id'); ?></div></div>
		<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('title', array('class' => 'form-control')); ?></div></div>
		<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('details', array('class' => 'form-control')); ?></div></div>
		<?php echo $this->Form->input('start', array('id' => 'startdate_id')); ?>
		<?php echo $this->Form->input('end', array('id' => 'enddate_id')); ?>
		<div class="row"><div class="col-lg-12"><?php echo $this->Form->input('all_day', array('checked' => 'checked')); ?></div></div>
		<div class="row"><div class="col-lg-12"><?php echo $this->Form->submit('Submit', array('class' => 'btn btn-primary'));?></div></div>
	</fieldset>
<?php echo $this->Form->end();?>
</div>
<div class="btn-group">
	<?php echo $this->Html->link(__('Manage Matches', true), array('plugin' => 'full_calendar', 'action' => 'index'), array('class' => 'btn btn-default'));?>
	<?php echo $this->Html->link(__('View Calendar', true), array('plugin' => 'full_calendar', 'controller' => 'full_calendar'), array('class' => 'btn btn-default')); ?>
</div>
</html>



