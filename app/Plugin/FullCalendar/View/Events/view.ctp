<?php
/*
 * View/Events/view.ctp
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */
?>
<div class="events view">
	<h2><?php echo __('Match'); ?></h2>
	<div class="container">
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('Match Type'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo $this->Html->link($event['EventType']['name'], array('controller' => 'event_types', 'action' => 'view', $event['EventType']['id'])); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('Commander'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo $event['User']['username']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('Title'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo $event['Event']['title']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('Details'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo $event['Event']['details']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('Start'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo $event['Event']['start']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('End'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php if($event['Event']['all_day'] != 1) { echo $event['Event']['end']; } else { echo "N/A"; } ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('All Day'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php if($event['Event']['all_day'] == 1) { echo "Yes"; } else { echo "No"; } ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('Created'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo $event['Event']['created']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo __('Modified'); ?>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
				<?php echo $event['Event']['modified']; ?>
			</div>
		</div>
	</div>
</div>

<?php if($current_user == $event['User']['id']):?>
<div class="page-header"><h1>Roster</h1></div>
<div class="container">
	<div class="row">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Username</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($user_list as $uid => $uname): ?>
				<tr>
					<td><?php echo $uname; ?></td>
					<td>
						<?php if($uid != $event['User']['id']): ?>
							<?php echo $this->Form->create('remove'); ?>
								<fieldset>
									<?php echo $this->Form->input('User.id', array('default' => $uid)); ?>
									<?php echo $this->Form->submit('Remove', array('class' => 'btn btn-default btn-xs')); ?>
								</fieldset>
							<?php echo $this->Form->end(); ?>
						<?php endif;?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif; ?>

<p>
<?php if ($current_user != $event['User']['id']): ?>
	<?php if (!in_array($current_user, $squad_members)) { ?>
		<?php echo $this->Form->create('Event'); ?>
			<fieldset>
				<?php echo $this->Form->input('id'); ?>
				<?php echo $this->Form->submit('Join Match', array('class' => 'btn btn-primary')); ?>
			</fieldset>
			<?php echo $this->Form->end(); ?>
	<?php } elseif (in_array($current_user, $squad_members)) { ?>
	<?php echo $this->Form->create('Event'); ?>
		<fieldset>
			<?php echo $this->Form->input('id'); ?>
			<?php echo $this->Form->submit('Leave Match', array('class' => 'btn btn-primary')); ?>
		</fieldset>
		<?php echo $this->Form->end(); ?>
	<?php } ?>
<?php endif; ?>
</p>

<p>
	<?php echo $this->Form->create('Map', array('url' => array('controller' => 'Maps', 'action' => 'map', 'plugin' => ''))); ?>
		<fieldset>
			<?php echo $this->Form->input('Squad.match_string', array('default' => $event['Event']['title'], 'type' => 'hidden')); ?>
			<?php echo $this->Form->input('Squad.event_id', array('default' => $event['Event']['id'], 'type' => 'hidden')); ?>
			<?php echo $this->Form->submit('Switch to match', array('class' => 'btn btn-success')); ?>
		</fieldset>
	<?php echo $this->Form->end(); ?>
</p>


<div class="btn-group">
<?php echo $this->Html->link(__('Edit Match', true), array('plugin' => 'full_calendar', 'action' => 'edit', $event['Event']['id']), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('Delete Match', true), array('plugin' => 'full_calendar', 'action' => 'delete', $event['Event']['id']), array('class' => 'btn btn-default'), sprintf(__('Are you sure you want to delete this %s match?', true), $event['EventType']['name'])); ?>
<?php echo $this->Html->link(__('Manage Matches', true), array('plugin' => 'full_calendar', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('View Calendar', true), array('plugin' => 'full_calendar', 'controller' => 'full_calendar'), array('class' => 'btn btn-default')); ?>
</div>
