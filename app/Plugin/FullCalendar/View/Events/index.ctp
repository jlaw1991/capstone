<?php
/*
 * View/Events/index.ctp
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */
?>
<div class="events index">
	<h2><?php echo __('Matches');?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
		<thead>
			<tr>
					<th><?php echo $this->Paginator->sort('event_type_id');?></th>
					<th><?php echo $this->Paginator->sort('title');?></th>
					<th><?php echo $this->Paginator->sort('commander');?></th>
					<th><?php echo $this->Paginator->sort('start');?></th>
		            <th><?php echo $this->Paginator->sort('end');?></th>
		            <th><?php echo $this->Paginator->sort('all_day');?></th>
					<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php
		$i = 0;
		foreach ($events as $event):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td>
				<?php echo $this->Html->link($event['EventType']['name'], array('controller' => 'event_types', 'action' => 'view', $event['EventType']['id'])); ?>
			</td>
			<td><?php echo $event['Event']['title']; ?></td>
			<td><?php echo $event['User']['username']; ?></td>
			<td><?php echo $event['Event']['start']; ?></td>
	        <?php if($event['Event']['all_day'] == 0) { ?>
	   		<td><?php echo $event['Event']['end']; ?></td>
	        <?php } else { ?>
			<td>N/A</td>
	        <?php } ?>
	        <td><?php if($event['Event']['all_day'] == 1) { echo "Yes"; } else { echo "No"; } ?>&nbsp;</td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('action' => 'view', $event['Event']['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $event['Event']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			</td>
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<div class="well">
		<p>Choose the commander that you wish to follow and then click on the <b>view</b> button on that commander's row. Afterwards you may join the match setup by the commander by clicking the join button.</p>
	</div>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="btn-group">
<?php echo $this->Html->link(__('New Match', true), array('plugin' => 'full_calendar', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('Manage Match Types', true), array('plugin' => 'full_calendar', 'controller' => 'event_types', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('View Calendar', true), array('plugin' => 'full_calendar', 'controller' => 'full_calendar'), array('class' => 'btn btn-default')); ?>
</div>
