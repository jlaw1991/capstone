<?php
/*
 * Views/EventTypes/edit.ctp
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */
?>
<div class="eventTypes form">
<?php echo $this->Form->create('EventType');?>
	<fieldset>
 		<legend><?php echo __('Edit Match Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('class' => 'form-control'));
		echo $this->Form->input('color', 
					array('options' => array(
						'Blue' => 'Blue',
						'Red' => 'Red',
						'Pink' => 'Pink',
						'Purple' => 'Purple',
						'Orange' => 'Orange',
						'Green' => 'Green',
						'Gray' => 'Gray',
						'Black' => 'Black',
						'Brown' => 'Brown'
					),
					'class' => 'form-control'));

	?>
	<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-primary')); ?>
	</fieldset>
<?php echo $this->Form->end();?>
</div>
<div class="row">
	<div class="btn-group">
		<?php echo $this->Html->link(__('View Match Type', true), array('plugin' => 'full_calendar', 'action' => 'view', $this->Form->value('EventType.id')), array('class' => 'btn btn-default')); ?>
		<?php echo $this->Html->link(__('Manage Match Types', true), array('plugin' => 'full_calendar', 'action' => 'index'), array('class' => 'btn btn-default'));?>
		<?php echo $this->Html->link(__('View Calendar', true), array('plugin' => 'full_calendar', 'controller' => 'full_calendar'), array('class' => 'btn btn-default')); ?>
	</div>
</div>
