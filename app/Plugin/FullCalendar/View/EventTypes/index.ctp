<?php
/*
 * View/EventTypes/index.ctp
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */
?>
<div class="eventTypes index">
	<h2><?php echo __('Match Types');?></h2>
	<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
		<thead>
			<tr>
					<th><?php echo $this->Paginator->sort('name');?></th>
		            <th><?php echo $this->Paginator->sort('color');?></th>
					<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$i = 0;
			foreach ($eventTypes as $eventType):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
			?>
			<tr<?php echo $class;?>>
				<td><?php echo $eventType['EventType']['name']; ?>&nbsp;</td>
		        <td><?php echo $eventType['EventType']['color']; ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View', true), array('plugin' => 'full_calendar', 'action' => 'view', $eventType['EventType']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
					<?php echo $this->Html->link(__('Edit', true), array('plugin' => 'full_calendar', 'action' => 'edit', $eventType['EventType']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="btn-group">
<?php echo $this->Html->link(__('New Match Type', true), array('plugin' => 'full_calendar', 'action' => 'add'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('Manage Matches', true), array('plugin' => 'full_calendar', 'controller' => 'events', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
<?php echo $this->Html->link(__('View Calendar', true), array('plugin' => 'full_calendar', 'controller' => 'full_calendar'), array('class' => 'btn btn-default')); ?>
</div>
