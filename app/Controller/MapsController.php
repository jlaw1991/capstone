<?php

// AuthComponent::user('id')
App::uses('File', 'Utility');
class MapsController extends AppController {

	public $helpers = array('Html', 'Session');
	
	public $uses = array(
        'User',
        'Image',
        'ImageGroup',
        'Squad',
        'SquadMember',
        'SquadSketch'
    );
    
// TEMPORARY FUNCTION
	public function map() {

 		$userId = $this->Auth->user('id');
 		$user_arr = $this->User->find('first', array('conditions' => array('id' => $userId)));
 		$sm_arr = $this->SquadMember->find('list', array('fields' => 'squad_id', 'conditions' => array('user_id' => $userId)));
 		$squad_arr = $this->Squad->find('list', array('fields' => array('id', 'match_string'), 'conditions' => array('id' => $sm_arr)));

 		$this->set('squads', $squad_arr);
		$this->set('sm_arr', $sm_arr);
 		// this is just to have drawings for the first squad only; will be replaced by stuff in if statement if possible
 		$squad_id = reset($sm_arr);

        if(empty($squad_id)) {
            $squad_id = 0;
        } else {
        	$match_id = $squad_arr[$squad_id];
        }
        if(empty($match_id)) {
        	$match_id = "";
        }

        if(!empty($this->data)) {
        	CakeLog::write('jsonlog', 'postdata from cal');
        	CakeLog::write('jsonlog', json_encode($this->request->data));
        	$match_id = $this->request->data['Squad']['match_string'];
        }

		$this->Session->write('User.id', $user_arr['User']['id']);
		$this->Session->write('User.role', $user_arr['User']['role']);
		$this->Session->write('User.squad', $squad_id);
		$this->Session->write('User.match_id', $match_id);
		$this->Session->write('User.region', 'na');
	
		//$this->set('user_arr', $user_arr);
		//$this->set('json_squad', json_encode($squad_arr));

	}


// TEMPORARY FUNCTION
private function getTempImageGroupIDs() {
	$ids = array();
 		//$ids[] = '1';
 		$ids[] = '2';
 		$ids[] = '3';
 		$ids[] = '4';
 		
 		return $ids;
	

}	
// DOES NOT WORK (can't create rows)	
public function uploadBase64() {

	$this->autoRender = false;
	if( !$this->request->is('ajax') ) {
		throw new MethodNotAllowedException('Method can only be called through Ajax requests'); 
	}
	
	$title = $this->request->data('title');
	$base64 = $this->request->data('base64');
	$tlx = $this->request->data('tlx');
	$tly = $this->request->data('tly');
	$brx = $this->request->data('brx');
	$bry = $this->request->data('bry');
	
	$userfolder = $this->Session->read('User.id');
	
	$folder_url = APP.'sketches/'.$userfolder;
	
	// create the folder if it does not exist
	if(!is_dir($folder_url)) {
		mkdir($folder_url);
	}
	
	$img = str_replace('data:image/png;base64,', '', $base64);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$filename = uniqid() . '.png';
	$success = file_put_contents($folder_url.'/'.$filename, $data);
	
	if(!$success) return null;
	
	/* foreign key constraint */

	$this->ImageGroup->create();
	$ig_data = array('user_id' => $userfolder, 'title' => $title);
	$this->ImageGroup->set($ig_data);
	$this->ImageGroup->save();
	
	$this->Image->create();
	$i_data = array('image_group_id' => $this->ImageGroup->getLastInsertID(), 'filepath' => $userfolder.'/'.$filename, 'tlx' => $tlx, 'tly' => $tly, 'brx' => $brx, 'bry' => $bry);
	$this->Image->set($i_data);
	$this->Image->save(); 
	
	if(!$this->Session->check('User.squad') || $this->Session->read('User.squad') != 0) {
		$squad = $this->Session->read('User.squad');
		$this->SquadSketch->create();
		$ss_data = array('squad_id' => $squad, 'image_group_id' => $this->ImageGroup->getLastInsertID());
		$this->SquadSketch->set($ss_data);
		$this->SquadSketch->save($ss_data);
	}
	
	$retval = array('image_id'=> $this->Image->getLastInsertID(), 'image_group_id'=>$this->ImageGroup->getLastInsertID());

	echo json_encode($retval);
}

public function getServerAndMatch() {
	$this->autoRender = false;
	// $isCommanderWithSquad = [hasSquad]|[isCommander]

	$isCommanderWithSquad = 0;
	if($this->Session->check('User.squad') and $this->Session->read('User.squad') > 0) {
		if($this->Session->read('User.role') == 'commander') $isCommanderWithSquad = 3;
		else $isCommanderWithSquad = 2;
	} else if($this->Session->read('User.role') == 'commander') $isCommanderWithSquad = 1;
	
	if($this->Session->check('User.region') and $this->Session->check('User.match_id')) {
		$retval = array('region'=> $this->Session->read('User.region'), 'match_id'=>$this->Session->read('User.match_id'), 'squad_set'=>$isCommanderWithSquad);
		return json_encode($retval);	
	
	} else return null;

}

private function getSQLDate() {
	$this->autoRender = false;
	$date = new DateTime('now');
	$dateStr = $date->format("Y-m-d H:i:s"); 
	CakeLog::write('jsonlog', $dateStr);
	return $dateStr; 
}

public function deleteImageGroup($imggroup=null, $deleteImageFiles=true) {
	// delete imageGroup with id $id
	// Also delete any images that belong to that imagegroup, and any 
	// squadsketches corresponding to that imagegroup
	$this->autoRender = false;
	CakeLog::write("mergeImageGroups.log", "imggroup: $imggroup");
	$id = $imggroup;
	CakeLog::write("mergeImageGroups.log", "id: $id");
	$deleteFiles = $deleteImageFiles;
	

	/** CAUSES NULL ID ISSUE **/
	if($imggroup == null) {
		$id = $this->request->data('image_group_id');
		
		$deleteFiles = $this->request->data('deleteImageFiles');
		if($deleteFiles == null) $deleteFiles = true;
	}

	// seems like the ajax returns a null id
	CakeLog::write("mergeImageGroups.log", "id after ajax: $id");
	
	if($id==null)
		throw new MethodNotAllowedException('Method cannot be called with null $id'); 
	
	// Make sure that this imagegroup exists and this user owns it
	$img_owner = $this->ImageGroup->find('first', array(
  	'conditions'=>array('ImageGroup.id'=>$id),
  	'fields'=>array('ImageGroup.user_id')
	));
	
	if($img_owner["ImageGroup"]["user_id"] == $this->Session->read('User.id')) {
		if($deleteFiles) {
			// Find filepaths of all images that need to be deleted
			$imgs = $this->Image->find('all', array(
  				'conditions'=>array('Image.image_group_id'=>$id),
  				'fields'=>array('Image.filepath')));
			// Delete image files
			foreach($imgs as $img) {
				$file = new File(APP.'sketches/'.$img["Image"]["filepath"]);
				$file->delete();
			}
		}
		
		// Let database do cascading; more efficient than cakephp
		$this->ImageGroup->delete($id, false);
		
	}
	
}

// INCOMPLETE	
public function getUniqueName() {
	$this->autoRender = false;
	if( !$this->request->is('ajax') ) {
		throw new MethodNotAllowedException('Method can only be called through Ajax requests'); 
	}
	
	$name = $this->request->data('name');
	$ruser = $this->Session->read('User.id');
	
 	// just return original name for now
 	//TODO
 	//return $name; 
 
 	if($this->Session->read('User.role') == 'commander') {
 		/*
 		$count = 
 	
 		select count(ig.id) from image_groups ig, squad_sketches ss, squad_members sm 
 		where
		sm.user_id = $ruser and
		sm.squad_id = ss.squad_id and ss.image_group_id = ig.id and ig.title=$name;
		
 		if($count == 0) return $name;
 		return $name.'_('.$count.')';
 	
 		*/
 		$q = "SELECT count(ImageGroup.id) as NameCount FROM image_groups ImageGroup, squad_sketches SquadSketch, squad_members SquadMember WHERE SquadMember.user_id = ";
 		$q .= $ruser . " AND SquadMember.squad_id = SquadSketch.squad_id AND SquadSketch.image_group_id = ImageGroup.id AND ImageGroup.title = '" . $name . "';";

 		//CakeLog::write("sql", $q);

 		$count = $this->ImageGroup->query($q);

 		//CakeLog::write("sql", "if_before_count ".$name);
 		if($count == 0) return $name;
 		//CakeLog::write("sql", "if_after_count ".$name.'_('.$count[0][0]['NameCount'].')');
 		return $name.'_('.$count[0][0]['NameCount'].')';
 	
 	}

}

// DOES NOT WORK (can't create rows)
public function mergeImageGroups() {
	$this->autoRender = false;
	if( !$this->request->is('ajax') ) {
		throw new MethodNotAllowedException('Method can only be called through Ajax requests'); 
	}
	
	$title = $this->request->data('title');
	$ids = $this->request->data('ids');

	CakeLog::write("mergeImageGroups.log", $title);
	CakeLog::write("mergeImageGroups.log", json_encode($ids));

	$ruser = $this->Session->read('User.id');

	// squads this user belongs to
	$squads = $this->SquadMember->find('all', array('conditions' => array('SquadMember.user_id' => $ruser), 'fields'=>array('SquadMember.squad_id')));
	$squads_arr = array();
	foreach($squads as $s) {
		$squads_arr[] = $s["SquadMember"]["squad_id"];
	}
	// User may combine any imagegroups he may view
	foreach($ids as $g) {
		// Don't need to check squad-related permissions if 
		// this user is the imagegroup owner
		CakeLog::write("mergeImageGroups.log", "ImageGroup.id " . $g);
		$found = $this->ImageGroup->find('first', array('conditions'=>array('ImageGroup.id'=>$g, 'ImageGroup.user_id'=>$ruser)));
		if($found != null) break;
		foreach($squads_arr as $s) {
			$found = $this->SquadSketch->find('first', array('conditions'=>array('SquadSketch.squad_id'=>$s, 'SquadSketch.image_group_id'=>$g)));
			if($found != null) break;
		}	
		if($found == null) return null;
	} 
	CakeLog::write("mergeImageGroups.log", "out of foreach");
	$this->ImageGroup->create();
	$ig_data = array('title' => $title, 'user_id' => $ruser);
	$this->ImageGroup->set($ig_data);
	$this->ImageGroup->save();
	CakeLog::write("mergeImageGroups.log", "after ig_data save");
	$newgroup = $this->ImageGroup->getLastInsertID();
	CakeLog::write("mergeImageGroups.log", "newgroup: " . $newgroup);
	
	if($this->Session->check('User.squad')) {
		$squad = $this->Session->read('User.squad');
		$this->SquadSketch->create();
		$ss_data = array('squad_id' => $squad, 'image_group_id' => $newgroup);
		$this->SquadSketch->set($ss_data);
		$this->SquadSketch->save();
		CakeLog::write("mergeImageGroups.log", "SquadSketch->save()");
	}
	
	// Change image_group_id of all relevant images, 
	// delete all old imagegroups and squadsketches

	foreach($ids as $g) {
		CakeLog::write("mergeImageGroups.log", "old group id: " . $g);
		$this->Image->updateAll(array('Image.image_group_id' => $newgroup), array('Image.image_group_id'=>$g));
		$this->deleteImageGroup($g, false);
		
	}
	CakeLog::write("mergeImageGroups.log", "Changed image_group_id of all relevant images");
	
	return $newgroup;
}

// INCOMPLETE
private function getAllImageGroupIDs() {
	$this->autoRender = false;
	$ruser = $this->Session->read('User.id');
	//$sid = $this->Session->read('User.squad');
 
 	if($this->Session->read('User.role') == 'commander') {
 		
 		//$ids = $this->getTempImageGroupIDs();

 		/*
 		$ids = 
 		select ss.image_group_id from squad_sketches ss, squad_members sm where
		sm.user_id = $ruser and
		sm.squad_id = ss.squad_id;
		*/
		//CakeLog::write("sql", "getAllImageGroupIDs");

		$q = "SELECT SquadSketch.image_group_id FROM squad_sketches SquadSketch, squad_members SquadMember WHERE ";
		$q .= "SquadMember.user_id = " . $ruser . " AND SquadMember.squad_id = SquadSketch.squad_id";

		//CakeLog::write("sql", $q);

		$ids = $this->SquadSketch->query($q);

		/*CakeLog::write("sql", "query exec");

		CakeLog::write("sql", json_encode($ids));

		for($i = 0; $i < count($ids); $i++) {
			CakeLog::write("sql", $ids[$i]);
		}*/

		return $ids;
 	
 	} else {
 		$squad = $this->Session->read('User.squad');
 		
 		$ids = $this->SquadSketch->find('all', array('conditions'=>array('SquadSketch.squad_id' => $squad), 'fields'=>array('SquadSketch.image_group_id')));

	CakeLog::write('jsonlog', 'getAllImageGroupIDs');

		$ids_arr = array();
		foreach($ids as $id) {
			$ids_arr[] = $id["SquadSketch"]["image_group_id"];
		}
		
		CakeLog::write('jsonlog', json_encode($ids));
		CakeLog::write('jsonlog', json_encode($ids_arr));
		
 		return $ids_arr;
 	
 	} 

}

// INCOMPLETE
public function getAllImageGroups() {
	$this->autoRender = false;
	$ruser = $this->Session->read('User.id');
	//$sid = $this->Session->read('User.squad');
 	
 	if($this->Session->read('User.role') == 'commander') {
 		
 		/*
 		$ids = 
 		select ss.image_group_id from squad_sketches ss, squad_members sm where
		sm.user_id = $ruser and
		sm.squad_id = ss.squad_id;
		*/
		
		//CakeLog::write("sql", "after query");

		$x = "SELECT ImageGroup.id FROM image_groups ImageGroup WHERE ";
		$x .= "ImageGroup.user_id = " . $ruser . ";";

		CakeLog::write("sql", $x);

		$ids_owned = $this->SquadSketch->query($x);
		CakeLog::write("sql", json_encode($ids_owned));

		$imgs_owned = array();
		foreach($ids_owned as $g) {
			$imgs_owned[] = $g["ImageGroup"]["id"];
		}
		CakeLog::write("sql", json_encode($imgs_owned));

		if($this->Session->check('User.squad') and $this->Session->read('User.squad') > 0) {
			$q = "SELECT SquadSketch.image_group_id FROM squad_sketches SquadSketch, squad_members SquadMember WHERE ";
			$q .= "SquadMember.user_id = " . $ruser . " AND SquadSketch.squad_id = SquadMember.squad_id;";

			CakeLog::write("sql", $q);

			$ids_squad = $this->SquadSketch->query($q);
			CakeLog::write("sql", json_encode($ids_squad));
			$bold = array();
 			$regular = array();
			$squad = $this->Session->read('User.squad');
			
			$imgs_squad = array();
			foreach($ids_squad as $g) {
				$imgs_squad[] = $g["SquadSketch"]["image_group_id"];
			}

			$imgs_all = array_unique(array_merge($imgs_squad, $imgs_owned), SORT_REGULAR);

			$bold_regular = $this->getImageGroupsWithTitle($imgs_all);

			foreach($bold_regular as $i) {
				//CakeLog::write("sql", "val: " . $g["SquadSketch"]["image_group_id"]);

				$inSquad = $this->SquadSketch->find('first', array('conditions' => array('SquadSketch.squad_id' => $squad, 'SquadSketch.image_group_id' => $i["image_group_id"])));
				if($inSquad == null) {
					$regular[] = array('image_group_id' => $i["image_group_id"], 'title' => $i["title"]);
				} else $bold[] = array('image_group_id' => $i["image_group_id"], 'title' => $i["title"]);
			}
			
			$retval = array('regular' => $regular, 'bold' => $bold);
			return json_encode($retval);
			
		} else {
			

 			$reg = $this->getImageGroupsWithTitle($imgs_owned);
 			$retval = array('regular' => $reg);
 			return json_encode($retval);
 		}
 	} else {
 		CakeLog::write('jsonlog', 'getAllImageGroups');
 		$squad = $this->Session->read('User.squad');
 		
 		$ids = $this->SquadSketch->find('all', array('conditions'=>array('SquadSketch.squad_id' => $squad), 'fields' => array('SquadSketch.image_group_id')));
		
		$ids_arr = array();
		foreach($ids as $id) {
			$ids_arr[] = $id["SquadSketch"]["image_group_id"];
		}
		
		CakeLog::write('jsonlog', json_encode($ids));
		CakeLog::write('jsonlog', json_encode($ids_arr));
		
		$reg = $this->getImageGroupsWithTitle($ids_arr);
 		$retval = array('regular' => $reg);
 		return json_encode($retval);
 	
 	} 
	
}

private function getImageGroupsWithTitle($ids) {
	$regular = array();
	foreach($ids as $g) {
 		$title = $this->ImageGroup->find('first', array('conditions'=>array('ImageGroup.id'=>$g), 'fields'=>array('ImageGroup.title')));
 		$regular[] = array('image_group_id'=>$g, 'title'=> $title["ImageGroup"]["title"]);
 	}
 	return $regular;
}

public function getImages() {
	$this->autoRender = false;
	if( !$this->request->is('ajax') ) {
		throw new MethodNotAllowedException('Method can only be called through Ajax requests'); 
	}
	
	$id = $this->request->data('image_group_id');

	// Execute QUERY #1, return result
	$imgs = $this->Image->find('all', array(
  	'conditions'=>array('Image.image_group_id'=>$id),
  	'fields'=>array('Image.id', 'Image.tlx', 'Image.tly', 'Image.brx', 'Image.bry')
	));
	
	return json_encode($imgs);

}

// INCOMPLETE
public function getImageFile($id) {

	$this->autoRender = false;

	$uid = $this->Session->read('User.id');
	$sid = $this->Session->read('User.squad');
	
	// TEMPORARY	
	//$filepath = $this->Image->find('first', array('conditions'=>array('Image.id'=>$id), 'fields'=>array('Image.filepath')));
	
	// END TEMPORARY
	
	/*
	$filepath = 
	
	select i.filepath from images i where i.id = $id and exists (
	select * from squad_sketches ss, squad_members sm where
		sm.user_id = $uid and 
		i.image_group_id = ss.image_group_id and 
		sm.squad_id = ss.squad_id ); 
	*/
	// TODO


	if(!$this->Session->check('User.squad') || $this->Session->read('User.squad') != 0) {
		$q = "SELECT Image.filepath FROM images Image WHERE Image.id = " . $id . " AND (exists(";
		$q .= "SELECT * FROM squad_sketches SquadSketch, squad_members SquadMember WHERE SquadMember.user_id = " . $uid;
		$q .= " AND Image.image_group_id = SquadSketch.image_group_id AND SquadMember.squad_id = SquadSketch.squad_id) OR exists(";
		$q .= "SELECT * FROM image_groups ImageGroup, images Image WHERE Image.id = $id AND Image.image_group_id = ImageGroup.id AND ";
		$q .= "ImageGroup.user_id = $uid));";

		$filepath = $this->Image->query($q);

		CakeLog::write('filepath_test', $q);
		CakeLog::write('filepath_test', json_encode($filepath));
		CakeLog::write('filepath_test', $filepath[0]["Image"]["filepath"]);

		$this->response->type('png');


		if(empty($filepath) || $filepath == null) return null;	
		$this->response->file(APP.'sketches/'.$filepath[0]["Image"]["filepath"]);
		
		return $this->response;
	} else {
		$q = "SELECT Image.filepath FROM images Image WHERE Image.id = " . $id . " AND exists(";
		$q .= "SELECT * FROM image_groups ImageGroup WHERE ImageGroup.user_id = $uid AND Image.image_group_id = ImageGroup.id);";

		$filepath = $this->Image->query($q);

		CakeLog::write('filepath_test', $q);
		CakeLog::write('filepath_test', json_encode($filepath));
		CakeLog::write('filepath_test', $filepath[0]["Image"]["filepath"]);

		$this->response->type('png');


		if(empty($filepath) || $filepath == null) return null;	
		$this->response->file(APP.'sketches/'.$filepath[0]["Image"]["filepath"]);
		
		return $this->response;		
	}
	
	
}

// PARTIALLY WORKS: query for $added returns nothing
// TODO 
public function updateMapSession() {
	$this->autoRender = false;
	CakeLog::write('jsonlog', 'updateMapSession');
	if($this->Session->read('User.role') == 'commander' or !$this->Session->check('User.squad') or $this->Session->read('User.squad') <= 0) return null;	
	
	$squad = $this->Session->read('User.squad');
	
	// Find deleted imagegroups
	$deleted = array();
	$groups = $this->Session->read('User.imageGroups');
	foreach($groups as $g) {
		$f = $this->SquadSketch->find('first', array(
  	'conditions'=>array('SquadSketch.image_group_id'=>$g, 'SquadSketch.squad_id'=>$squad)));
		if(!count($f)) $deleted[] = $g;
	} 
	
	// Find added imagegroups
	$added = $this->SquadSketch->find('all', array(
  	'conditions'=>array('SquadSketch.squad_id'=>$squad, 'SquadSketch.created >'=> $this->Session->read('User.last_update')),
  	'fields'=>array('SquadSketch.image_group_id')
	));

	/*$added_query = "SELECT SquadSketch.image_group_id FROM squad_sketches SquadSketch WHERE SquadSketch.created > '" . $this->Session->read('User.last_update') ."';";
	CakeLog::write('jsonlog', 'added_query');
	CakeLog::write('jsonlog', $added_query);
	$added = $this->SquadSketch->query($added_query);
	CakeLog::write('jsonlog', 'added');
	CakeLog::write('jsonlog', json_encode($added));*/

	$added_arr = array();
	foreach($added as $a) {
		$added_arr[] = $a["SquadSketch"]["image_group_id"];
	}
	
	// Finally, update User.imageGroups and User.last_update
	$this->Session->write('User.imageGroups', $this->getAllImageGroupIDs());
	$this->Session->write('User.last_update', $this->getSQLDate());
	
	$retval = array('added' => $this->getImageGroupsWithTitle($added_arr), 'deleted' => $deleted);
	return json_encode($retval);
}
	
public function beginMapSession() {
	$this->autoRender = false;
	if( !$this->request->is('ajax') ) {
		throw new MethodNotAllowedException('Method can only be called through Ajax requests'); 
	}
	$role = $this->Session->read('User.role');
	CakeLog::write('jsonlog', 'beginMapSession');
	// Non-commanders keep a last_update time and a list of displayed imagegroups,
	// used by updateMapSession
	if($role != 'commander' and $this->Session->check('User.squad') and $this->Session->read('User.squad') > 0) {
		$date = $this->getSQLDate();
		$this->Session->write('User.last_update', $date);
		$img_groups = $this->getAllImageGroupIDs();
		$this->Session->write('User.imageGroups', $img_groups);
	}	
	// A commander's map session is updated by the commander;
	// no need to store info for updateMapSession
	$retval = $this->getAllImageGroups();
	CakeLog::write('jsonlog', 'ret of beginMapSession');
	CakeLog::write('jsonlog', $retval);
	return $retval;
	
}


public function getMatchStats() {
	$this->autoRender = false;
	header('Content-type: application/json; charset=utf-8');
	$stuff = file_get_contents("http://gw2stats.net/api/matches.json");
	//CakeLog::write('jsonlog', $stuff);
	echo  $stuff; 	
}

public function getObjStats() {
	$this->autoRender = false;
	
	if( !$this->request->is('ajax') ) {
		throw new MethodNotAllowedException('Method can only be called through Ajax requests'); 
	}
	
	$matchid = $this->request->data('matchid');
	$url = "http://gw2stats.net/api/objectives.json?id=" . $matchid . "&type=match";
	
	//CakeLog::write('jsonlog', $url);
	
	
	header('Content-type: application/json; charset=utf-8');
	$stuff = file_get_contents($url);
	
	echo  $stuff; 	
}


	
}
?>
