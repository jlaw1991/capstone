if(typeof(L) !== 'undefined') {
 
var BLOODLUST_IMG_SIZE = "23";

function updateScoresHeader() {
	
	var i, j, color, score, ppt;
	var max_score = 0;
	var max_ppt = 0;
	var bloodlust_colors = ["red", "green", "blue"];
	
	// Delete all existing bloodlust
	var lust = document.getElementsByClassName("bloodlust");
	
	while(lust.length > 0) {
		lust[0].parentNode.removeChild(lust[0]);
		lust = document.getElementsByClassName("bloodlust");
	}
	
	// First get max score and ppt
	for(i=0; i<wvwMatch["worlds"].length; i++) {
		if(wvwMatch["worlds"][i]["score"] > max_score)
			max_score = wvwMatch["worlds"][i]["score"];
		if(wvwMatch["worlds"][i]["ppt"] > max_ppt)
			max_ppt = wvwMatch["worlds"][i]["ppt"];
	}

	for(i=0; i<wvwMatch["worlds"].length; i++) {
		color = wvwMatch["worlds"][i]["color"];
	
		// Set army score
		score = wvwMatch["worlds"][i]["score"];
		document.getElementById(color + "_score").innerHTML = score;
		document.getElementById(color + "_score_bar").setAttribute("style", "width: " + Math.round(score/max_score*100) + "%;"); 
		
		// Set army ppt
		ppt = wvwMatch["worlds"][i]["ppt"];
		document.getElementById(color + "_ppt").innerHTML = ppt + " PPT";
		document.getElementById(color + "_ppt_bar").setAttribute("style", "width: " + Math.round(ppt/max_ppt*100) + "%;"); 
		
	
		// Set bloodlust 
		for(j=0; j<bloodlust_colors.length; j++) {
			if(wvwMatch["bloodlust"][bloodlust_colors[j] + "_owner_id"] == wvwMatch["worlds"][i]["world_id"]) {
			
				lust = document.createElement("img");
				lust.className="bloodlust";
				lust.height = BLOODLUST_IMG_SIZE;
				lust.src = bloodlustFolder + bloodlust_colors[j] + '.png';
				
				document.getElementById(color + "_stats").appendChild(lust);		
			 
			}
		}
				
	}
}

}