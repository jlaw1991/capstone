
if(typeof(L) !== 'undefined') {

var defaultSize; 
var largeSize;

function mouseIn(e) {
	e.style.width = largeSize + 'px';
	e.style.height = largeSize + 'px';
}


function mouseOut(e) {
	e.style.width = defaultSize + 'px';
	e.style.height = defaultSize + 'px';
	e.src = saveIconFolder + 'save-icon.png';
}

function mouseUp(e) {
	e.src = saveIconFolder + 'save-icon.png';
}

function mouseDown(e) {
	e.src = saveIconFolder + 'save-icon-1.png';
}



var SaveControl = L.Control.extend({
    options: {
        position: 'topright'
    },

    onAdd: function (map) {
        // create the control container with a particular class name
        var container = L.DomUtil.create('div', 'my-save-class');      
        
        defaultSize = document.getElementsByClassName('leaflet-control-layers-toggle')[0].clientWidth; 
        	
        largeSize = defaultSize * 1.2;

        // ... initialize other DOM elements, add listeners, etc.
		$(container).append(
		"<img id='save-icon' src=\"" + saveIconFolder + "save-icon.png\" style='width:" + defaultSize + "px; height:" + defaultSize + "px; cursor: pointer' onclick='showPopup();' onmouseover='mouseIn(this);' onmouseout='mouseOut(this);' onmouseup='mouseUp(this)' onmousedown='mouseDown(this)'>");
				
        return container;
    }
});
}