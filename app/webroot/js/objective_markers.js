if(typeof(L) !== 'undefined') {

var debugCaptureTime = "2014-02-28 09:20:00 GMT";

var objMarkers = new Array();
var objMarkersWithLabel = new Array();

var MAX_RI_SECS = 300;
var GUILD_IMG_SIZE = "42";
var Z_OFFSET_MAG = 17000; // must exceed max possible latitude

// CSS tags
var objTextClass = "obj_text";
var objNameClass = "obj_name";
var objTimeClass = "obj_time_owned";
var objGuildClass = "obj_guild";
var objGuildNameClass = "obj_guild_name";
var objPtsClass = "obj_pts";

var iconNames = ['battleshollow', 'bauersestate', 'carversascent', 'orchardoverlook', 'templeoflostprayers', 'camp', 'castle', 'tower', 'keep'];
var colorNames = ['_white.png', '_red.png', '_green.png', '_blue.png']; 
var white = 0;
var red = 1;
var green = 2;
var blue = 3;

var icons = new Array();
var guildIcons = new Array();
var nullString = "null";

function getIcon(marker, color) {
	var i = marker.icon_type*colorNames.length + color;
	if(marker.guild_id == nullString) {
		return icons[i];
	}
	return guildIcons[i]; 
}

var ObjIcon = L.Icon.extend({
	options: {
		riseOnHover: true,
		iconSize: [26, 26],
		iconAnchor: [13, 13],
		popupAnchor: [0, 0],
		labelAnchor: [-10, 0]
	}
});

var GuildIcon = L.Icon.extend({
	options: {
		iconUrl: iconFolder + 'guild.png',
		iconSize: [30, 30],
		iconAnchor: [15, 15],
		shadowSize: [26, 26],
		shadowAnchor: [13, 13],
		popupAnchor: [0, 0],
		labelAnchor: [-10, 0]
	}
});

// From http://stackoverflow.com/questions/11660930/javascript-invalid-date
function parseCaptureTime(date) {
	var m = /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d) GMT$/.exec(date);
    var d = new Date();
    d.setUTCFullYear(m[1]);
    d.setUTCMonth(m[2]-1);
    d.setUTCDate(m[3]);
    d.setUTCHours(m[4]);
    d.setUTCMinutes(m[5]);
    d.setUTCSeconds(m[6]);
	return d;
}

function createIcons() {
	var i, j;
	for(i=0; i<iconNames.length; i++) {
		for(j=0; j<colorNames.length; j++) {
			icons[i*colorNames.length+j] = new ObjIcon({iconUrl: iconFolder + iconNames[i] + colorNames[j]});
			
			guildIcons[i*colorNames.length+j] = new GuildIcon({shadowUrl: iconFolder + iconNames[i] + colorNames[j]});

		}
	}
}
			

function createMarkerPopup(marker, name, pts) {
	var content = '<span class=\"' + objTextClass + ' ' + objNameClass + '\">' + name + '</span><span class=\"' + objTextClass + ' ' + objPtsClass + '\">&nbsp&nbsp&nbsp' + pts + '&nbsp pts</span><br /><span class=\"' + objTextClass + ' ' + objTimeClass + '\">0 m</span><br /><div class=\"' + objGuildClass + '\"></div>';
	
	marker.bindPopup(content);
}

function getTimeString(time) {
	var ret = "";
	var str = time.split(":");
	
	var days = parseInt(str[0]);
	if(days > 3) return days.toString() + ' d';
	if(days > 0) ret += days.toString() + ' d ';
	
	var hours = parseInt(str[1]);
	if(days > 0 || hours > 3) return ret + hours.toString() + ' h';
	if(hours > 0) ret += hours.toString() + ' h ';
	
	var mins = parseInt(str[2]);
	// Round to nearest 5 min
	mins = 5*Math.round(mins/5);
	
	if(mins == 0 && hours == 0) return '< 5 m';
	return ret + mins.toString() + ' m';
}
		 
	

function updateMarkerPopup(marker) {
	var scratch = document.getElementById("scratch");
		
	var obj = objStatus["maps"][marker.obj_map_id]["objectives"][marker.obj_id];
	
	// Leaflet doesn't allow marker content to be accessed as HTML,
	// so make a temporary spot for it
	scratch.innerHTML = marker.getPopup().getContent();
	
	// Update popup time held
	scratch.getElementsByClassName(objTimeClass)[0].innerHTML = "Owned for " + getTimeString(obj["time_held"]);
	
	// Update popup color
	var newClass = "";
	if(obj["current_owner"]["color"] == "BLUE") {
		newClass = " blue_text";
	} else if(obj["current_owner"]["color"] == "GREEN") {
		newClass = " green_text";
	} else if(obj["current_owner"]["color"] == "RED") {
		newClass = " red_text";
	}
	scratch.getElementsByClassName(objNameClass)[0].className = objTextClass + " " + objNameClass + newClass;
	
	// Update popup guild info
	if(obj.hasOwnProperty("current_guild")) {
		if(obj["current_guild"]["id"] != marker.guild_id) {
			scratch.getElementsByClassName(objGuildClass)[0].innerHTML  = '<span class=\"' + objTextClass + ' ' + objGuildNameClass + newClass + '\">' + obj["current_guild"]["name"] + '</span><img src=\"' + obj["current_guild"]["img_url"] + '\"width=\"' + GUILD_IMG_SIZE + '\" height=\"' + GUILD_IMG_SIZE + '\">';
			marker.guild_id = obj["current_guild"]["id"];
		}
	} else {
		if(marker.guild_id != nullString) {
			scratch.getElementsByClassName(objGuildClass)[0].innerHTML  = '';
			marker.guild_id = nullString;
		}
	}
	
	marker.setPopupContent(scratch.innerHTML);
	
}
	
function updateObjectives() {
	
	var objMap, i, obj;
	for(objMap = 0; objMap<objStatus["maps"].length; objMap++) {
	
		for(i=0; i<objMarkers.length; i++) {
		
			// Only check objectives for this map
			if(objMap != objMarkers[i].obj_map_id) continue;
		
			obj = objStatus["maps"][objMap]["objectives"][objMarkers[i].obj_id];
		
			// Exit if objective ownership has not changed
			if(objMarkers[i].owner_id == obj["current_owner"]["world_id"]) {
				updateMarkerPopup(objMarkers[i]);
				continue;
			}
			objMarkers[i].owner_id == obj["current_owner"]["world_id"];
			
			// Update capture time
			objMarkers[i].capture_time = obj["capture_time"];
		
			// Make sure countdown() updates label
			if(obj.hasOwnProperty("ri_remaining")) {
				objMarkers[i].has_ri = true;
				if(objMarkersWithLabel.indexOf(objMarkers[i]) == -1) {
					objMarkersWithLabel.push(objMarkers[i]);
				} 
			} else objMarkers[i].has_ri == false;
			
			
			// Update icon
			if(obj["current_owner"]["color"] == "RED") {
				objMarkers[i].setIcon(getIcon(objMarkers[i], red));
		
			} else if(obj["current_owner"]["color"] == "GREEN") {
				objMarkers[i].setIcon(getIcon(objMarkers[i], green));
			
			} else if(obj["current_owner"]["color"] == "BLUE") {
				objMarkers[i].setIcon(getIcon(objMarkers[i], blue));
			
			} else {
				objMarkers[i].setIcon(getIcon(objMarkers[i], white));
		
			}
			
			updateMarkerPopup(objMarkers[i]);
	
		}
	}	

}


function countdown() {

	var i;
	for(i=0; i<objMarkersWithLabel.length; i++) {
		
		// The marker should not have a label
		if(!objMarkersWithLabel[i].has_ri) {
		
			// Remove label and remove from list
			objMarkersWithLabel[i].setZIndexOffset(-Z_OFFSET_MAG);
			objMarkersWithLabel[i].unbindLabel();
			objMarkersWithLabel.splice(i, 1);
			i--;
			
		} else {
			var diffSecs = Math.abs(new Date() - parseCaptureTime(/*new Date(*/objMarkersWithLabel[i].capture_time/*.replace(/-/g, '/')*/))/1000;

			// The marker should have a label but finished counting down
			if(diffSecs > MAX_RI_SECS) {
			
				// Set has_ri to false; remove label and remove from list
				objMarkersWithLabel[i].has_ri = false;
				objMarkersWithLabel[i].setZIndexOffset(-Z_OFFSET_MAG);
				objMarkersWithLabel[i].unbindLabel();
				objMarkersWithLabel.splice(i, 1);
				i--;
				
			// The marker should have a label, and is still counting down
			} else {
				diffSecs =  MAX_RI_SECS - diffSecs;
			
				var mins = Math.floor(diffSecs/60);
				var secs = Math.floor(diffSecs - mins*60);
				
				var labelContent = mins.toString() + ':';

				if(secs<10) {
					labelContent = labelContent + '0';
				}
				
				labelContent = labelContent + secs.toString();
				
				// Add a label if the marker doesn't have one
				if(!objMarkersWithLabel[i].label) {
					objMarkersWithLabel[i].bindLabel(labelContent, {noHide: true});
					objMarkersWithLabel[i].setZIndexOffset(Z_OFFSET_MAG - objMarkersWithLabel[i].getLatLng()[0]);
					objMarkersWithLabel[i].showLabel();
				} else {
					objMarkersWithLabel[i].updateLabelContent(labelContent);
				}
			}
		}
	}
}
	

function addObjectiveMarkers(map) {
	createIcons();
	var marker, objID, type, name;

	for(objID in wvwObjectives) {
		
		name = wvwObjectives[objID]["name"]["en"];
		type = wvwObjectives[objID]["type"];
		if(type == "ruin") {
			if(name == "Temple of Lost Prayers") {
				type = "templeoflostprayers";
			} else if(name == "Battle's Hollow") {
				type = "battleshollow";
			} else if(name == "Bauer's Estate") {
				type = "bauersestate";
			} else if(name == "Carver's Ascent") {
				type = "carversascent";
			} else if(name == "Orchard Overlook") {
				type = "orchardoverlook";
			} else {
				// if we don't know what ruin this is, make it a camp
				type = "camp";
			}
		}
		
		marker = L.marker(unproject(wvwObjectives[objID]["coords"]), {title: name, zIndexOffset: -Z_OFFSET_MAG, riseOnHover: true, riseOffset: 2*Z_OFFSET_MAG});
		
		marker.obj_id = objID;
		marker.owner_id = 0; // neutral to start
		marker.guild_id = nullString;
		marker.has_ri = false;
		marker.obj_map_id = wvwObjectives[objID]["map_id"];
		
		marker.capture_time = objStatus["maps"][marker.obj_map_id]["objectives"][objID]["capture_time"];
		
		marker.icon_type = iconNames.indexOf(type);
		marker.setIcon(getIcon(marker, white));
		
		createMarkerPopup(marker, name,  wvwObjectives[objID]["score"].toString());
		
		marker.addTo(map);
		objMarkers.push(marker);
	}

}



}
