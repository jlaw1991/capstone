var sketchColors = ['#f00', '#ff0', '#0f0', '#0ff', '#00f', '#f0f', '#000', '#fff'];
var minBrushSize = 5;
var maxBrushSize = 30;
var brushSize = minBrushSize;

function setBrushSize(s) {
	
	if((s > 0 && brushSize >= maxBrushSize) || (s < 0 && brushSize <= minBrushSize)) { 
		console.log("new brush size: " + brushSize);
		return;	
	} 
	
	brushSize += s;
	
	// greyout 
	if(brushSize == minBrushSize) {
		document.getElementById("plus").style.color = "black";
		document.getElementById("minus").style.color = "silver";
		
	} else if(brushSize == maxBrushSize) {
		document.getElementById("plus").style.color = "silver";
		document.getElementById("minus").style.color = "black";
		
	} else {
		document.getElementById("plus").style.color = "black";
		document.getElementById("minus").style.color = "black";
	}

	if(brushSize < 10) {
		document.getElementById("brush").innerHTML = "&nbsp" + brushSize + "p" + "&nbsp";
	} else {	
		document.getElementById("brush").innerHTML = brushSize + "p";
	}
}

if(typeof(L) !== 'undefined') {

var SketchControl = L.Control.extend({
    options: {
        position: 'bottomleft'
    },

    onAdd: function (map) {
        // create the control container with a particular class name
        var container = L.DomUtil.create('div', 'tools leaflet-bar', document.getElementsByClassName("leaflet-tile-pane")[0]);        
        
        // ... initialize other DOM elements, add listeners, etc.
		
		// colors
		$.each(sketchColors, function() {
      		$(container).append("<a href='#" + canvasID + "' data-color='" + this + "' 	style='background: " + this + ";'></a> ");
    	});
    	
    	// eraser
		$(container).append("<a href='#" + canvasID + "' data-tool='eraser' style='background: #ccc;'>X</a> ");
		
		// divider
		$(container).append("<p></p>");

    	$(container).append("<a href='#" + canvasID + "' data-size='" + brushSize  + "' style='background: #ccc;' id='plus' onclick='setBrushSize(5)'>+</a> ");
		$(container).append("<span id='brush' style='background: #ccc; text-align:center; padding: 3px;'></span> ");
		$(container).append("<a href='#" + canvasID + "' data-size='" + brushSize  + "' style='background: #ccc;' id='minus' onclick='setBrushSize(-5)'>-</a> ");
		
		setBrushSize(0);		
        return container;
    }
});
}
