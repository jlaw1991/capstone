$( window ).resize(function() {
    setPopupSize();
});

$(document).keyup(function(e) {

  if (e.keyCode == 27) { // ESC
  
  	if(modalMode) {
  		
  		document.getElementById("cancel-button").className = "mybutton";
  		exitModal();	
  	
  	} else if(sketchMode) { 
  		console.log("exit sketch mode without uploading");
  	
		  exitSketchMode();
  	
  		drawBool = false;
  	}
  	
  } else if (e.keyCode == 13) {  // ENTER
  		if(modalMode) {
  			document.getElementById("save-button").className = "mybutton savebutton";
  			saveAsLayerGroup();
  		}	
  		
  } 
});

$(document).keydown(function(e) {

  if (e.keyCode == 27) { // ESC
  
  	if(modalMode) {
  		document.getElementById("cancel-button").className += " active-button";
  	} 
  } else if (e.keyCode == 13) {  // ENTER
  		if(modalMode) {
  			document.getElementById("save-button").className += " active-button";
  	
  		}	
  		
  } 
});