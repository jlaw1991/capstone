if(typeof(L) !== 'undefined') {
/*
function getBrowserZoom() {
	var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
	svg.setAttribute('version', '1.1');
	document.body.appendChild(svg);
	var z = svg.currentScale;
	//console.log("Z: " + z);
	document.body.removeChild(svg);
	return z;
}
*/
/*
var getNewSketch = function() { 			
 			imageBounds = [map.containerPointToLatLng([0,0]), 
 						map.containerPointToLatLng([map.getSize().x, map.getSize().y])]; 	
        		
        	console.log(imageBounds);
        			
        	clearSketch();
        		
        	// check last modified?
 			map.removeLayer(imageOverlay);
 			
        	imageOverlay = new L.ImageOverlay("file:///Users/minasavovic/Downloads/s3.png", imageBounds);
        	
        	map.addLayer(imageOverlay);
           
            exitSketchMode();
        
}
*/

var SketchLayer = L.CanvasLayer.extend({

	onAdd: function (map) {
		L.CanvasLayer.prototype.onAdd.call(this, map);
		
		var canvas = this.getCanvas();
        canvas.setAttribute('id', canvasID);
        
        $("#" + canvasID).sketch({defaultColor: "#ff0"});
    },
		
 
    render: function() {
        var canvas = this.getCanvas();
        
        var ctx = canvas.getContext('2d');
        canvas.bdraw = drawBool;
        //canvas.pbdraw = predrawBool;
        canvas.lmap = map; // unused; mysketch may need it 

        this.redraw();

    }
});

}
