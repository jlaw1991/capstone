var map;

// GW2 specs for WvWvW map 
var mapMinZoom = 2;
var mapMaxZoom = 6;
var mapSquareSize = 16384;
var mapClampedView = [5118, 6922];

var tileLayer, sketchLayer;
var tileLayerId, sketchLayerId;
var mySketchControl = new SketchControl();
var canvasID = "theSketcher";

var saveControl = new SaveControl();

var modalMode = false;
var sketchMode = false;

var drawBool = false;

var logStuff = function() {
	console.log("logging stuff:");
	
	
}

function unproject(coord) {
    return map.unproject(coord, map.getMaxZoom());
}

function clearSketch() {
	// clear canvas
    var canvas = document.getElementById(canvasID);
	canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
	// tell mysketch.js to clear current sketch, so it's not redrawn
	// when user re-enters sketch mode
	canvas.clearCanvas = true;
}

function onDblClick(e) {
    drawBool = !drawBool;
	if(is_commander) {
        if(drawBool != sketchMode) {
            if(drawBool) enterSketchMode();
            else {
                uploadNewSketch();
            }
        }
	}
}

function enterSketchMode() {
	sketchLayer.addTo(map);
	document.getElementById(canvasID).style.cursor="crosshair";

	map.dragging.disable();
	map.touchZoom.disable();
	map.boxZoom.disable();
    map.scrollWheelZoom.disable();
    map.keyboard.disable();  
        		
    map.addControl(mySketchControl);
    sketchMode = true;
}

function exitSketchMode() { 
    clearSketch();
	map.removeLayer(sketchLayer);
    map.removeControl(mySketchControl);
      	
    // enable map functions
    map.dragging.enable();
	map.touchZoom.enable();
    map.boxZoom.disable();
    map.scrollWheelZoom.enable();
    map.keyboard.enable();

    sketchMode = false;
    
}


// BEGIN MODAL

var exitModal = function() {
	$("#mask").hide();
    $("#popup").hide();
        
    modalMode = false;

}

function setPopupSize() {
    var ratio = 0.6;
    var minW = 300;
    var maxW = 900;
    
    var w = Math.round(ratio*window.innerWidth);
    
    if(w > maxW) {
    	w = maxW;
    } else if(w < minW) w = minW;

    $('#popup').css('width', w + 'px');

    $('#save-as').css('width', w - 270 + 'px');
}

function showPopup() {
    // show the mask
    $("#mask").fadeTo(500, 0.75);

	//setPopupSize();
	$("#save-as").val(nextName());
	
    // show the popup
    $("#popup").show();
    $("#save-as").focus();
    
    modalMode = true;
}

//END MODAL


$(function () {
	"use strict";
    
    var southWest, northEast;
    
    map = L.map("map", {
        minZoom: mapMinZoom,
        maxZoom: mapMaxZoom,
        zoomControl:false,
        crs: L.CRS.Simple
    }).setView([-mapSquareSize, 0], mapMinZoom, {reset: true});
    
    southWest = unproject([mapSquareSize, mapClampedView[1]]);
    northEast = unproject([mapClampedView[0], mapSquareSize]);
    
    map.setMaxBounds(new L.LatLngBounds(southWest, northEast));

    tileLayer = L.tileLayer("https://tiles.guildwars2.com/2/3/{z}/{x}/{y}.jpg", {
        minZoom: mapMinZoom,
        maxZoom: mapMaxZoom,
        continuousWorld: true
    }).addTo(map);
 
   	map.on("dblclick", onDblClick);
    
    map.on("click", logStuff);

    map.doubleClickZoom.disable();

	sketchLayer = new SketchLayer();

	tileLayerId = L.Util.stamp(tileLayer);
	sketchLayerId = L.Util.stamp(sketchLayer);

});