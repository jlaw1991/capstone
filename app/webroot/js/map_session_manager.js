if(typeof(L) !== 'undefined') {

var SESSION_UPDATE_RATE = 5000; // 120,000 ms = 2 minutes

function beginMapSession() {

	$.ajax({
		url: begin_map_session_url,
		dataType: "json",
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus + " Error: " + errorThrown);
        addJsonErrorMsg(0, "Problem contacting app server."); 
    }, 
		success: function (data) {
			addAllImageGroups(data, "regular", false);
			
			addAllImageGroups(data, "bold", true);
			
			if(squad_set && !is_commander) 
				setTimeout(updateMapSession, SESSION_UPDATE_RATE);
		}
	});

}

function updateMapSession() {
	$.ajax({
		url: update_map_session_url,
		dataType: "json",
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus + " Error: " + errorThrown);
        addJsonErrorMsg(5, "Problem retrieving commander updates."); 
    }, 
		success: function (data) {
			console.log("updating map session");
			console.log(data);
			if(data.hasOwnProperty("deleted"))
				deleteOverlaysByImageGroupID(data["deleted"]);
			
			addAllImageGroups(data, "added", false);
			
			setTimeout(updateMapSession, SESSION_UPDATE_RATE);
		}
	});

}

function addAllImageGroups(data, index, makeBold) {
	if(data.hasOwnProperty(index)) {
		for(var i=0; i<data[index].length; i++) {
			addImageGroup(data[index][i]["title"], data[index][i]["image_group_id"], makeBold);
		}
	}

}

function addImageGroup(title, id, makeBold) {
	$.ajax({
		url: get_images_url,
		type:'POST',
        cache: false,
		data: { image_group_id: id},
		dataType: "json",
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus + " Error: " + errorThrown); 
        addJsonErrorMsg(6, "Problem adding sketch."); 
    }, 
		success: function (data) {
			addOverlayFromDB(title, id, makeBold, data);
		}
	});
}

function addOverlayFromDB(title, image_group_id, makeBold, images) {
	var layer;
	if(images.length == 1) {
		var tl = [images[0]["Image"]["tlx"], images[0]["Image"]["tly"]];
		var br = [images[0]["Image"]["brx"], images[0]["Image"]["bry"]];
		layer = makeOverlayLayer(images[0]["Image"]["id"], tl, br);
	} else if(images.length > 1) {
		layer = new L.layerGroup();

		for(var i=0; i<images.length; i++) {
			var tl = [images[i]["Image"]["tlx"], images[i]["Image"]["tly"]];
			var br = [images[i]["Image"]["brx"], images[i]["Image"]["bry"]];
		
			layer.addLayer(makeOverlayLayer(images[i]["Image"]["id"], tl, br));
		}
	} else return;
	
	addNewOverlay(title, image_group_id, layer, makeBold);

}

function deleteOverlaysByImageGroupID(image_groups) {
	for(key in overlays) {
		if (arrayHasOwnIndex(overlays, key)) {
        	for(var i=0; i<image_groups.length; i++) {
        		if(overlays[key].image_group_id == image_groups[i]) {
        			localDeleteOverlay(key);
        			break;
        		}
        	}
    	}
	}

}

// http://stackoverflow.com/questions/9329446/for-each-in-an-array-how-to-do-that-in-javascript
function arrayHasOwnIndex(array, prop) {
    return array.hasOwnProperty(prop) && /^0$|^[1-9]\d*$/.test(prop) && prop <= 4294967294; // 2^32 - 2
}

}