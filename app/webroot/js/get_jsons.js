if(typeof(L) !== 'undefined') {

var MARKER_REFRESH_RATE = 10000;

var SQUAD_MSG_SUCCESS = "<span class='green_background'><span class='vs'>Squad set.&nbsp</span>";

var SQUAD_MSG_FAILURE = "<span class='red_background'><span class='vs'>No squad.&nbsp</span>";

var SQUAD_MSG_END = " Use the Calendar tab to review your squad settings.</span>";

var wvwObjectives;
var wvwMatch;
var objStatus;

var wvw_server_region;// = "na";
var match_id;// = "1-1";
var is_commander = false;
var squad_set = false;

function noMatchMsg() {
	document.getElementById("match_details").innerHTML = "Set a match on the Calendar tab to view match and objective details on the map.";
}

$(function () {

	$.ajax({
		url: get_server_and_match_url,
		dataType: "json",
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus + " Error: " + errorThrown);
        addJsonErrorMsg(0, "Problem contacting app server.");
 
    }, 
		success: function (data) {
	
			if(data == null) {
				 noMatchMsg();
			
			} else {
		
				wvw_server_region = data["region"];
				match_id = data["match_id"];
				if(data["squad_set"] == 3) {
					is_commander = true;
					squad_set = true;
					document.getElementById("squad_msg").innerHTML = SQUAD_MSG_SUCCESS + "Any sketches you create or save during this session will be shown to your current squad. You can check the layer control in the top right corner of the map to see which sketches are visible to your current squad - if the sketch title is in <b>bold font</b>, your squad can see it." + SQUAD_MSG_END;
				} else if(data["squad_set"] == 1) {
					is_commander = true;
					document.getElementById("squad_msg").innerHTML = SQUAD_MSG_FAILURE + "You have not set a squad. Any sketches you create or save during this session will be visible to you only." + SQUAD_MSG_END;
				} else if(data["squad_set"] == 2) { 
					squad_set = true;
					var rate = SESSION_UPDATE_RATE/1000;
					var rateStr = "";
					if(rate <= 60) {
						rateStr += Math.round(rate) + " seconds";
					} else {
						rateStr += Math.round(rate/60) + " minutes";
					}
					
					document.getElementById("squad_msg").innerHTML = SQUAD_MSG_SUCCESS + "New data from your commander will appear within " + rateStr + " or when you refresh this page." + SQUAD_MSG_END;
				} else {
					document.getElementById("squad_msg").innerHTML = SQUAD_MSG_FAILURE + "You won't see any sketches here because you haven't set a squad." + SQUAD_MSG_END;	
				
				}
				
				getMatchStatus();
				beginMapSession();
			}
		}
	});
	
});

function refreshMatchStatus() {
	$.ajax({
		url: get_match_stats_url,
		dataType: "json",
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus + " Error: " + errorThrown); 
        addJsonErrorMsg(1, "Problem updating match scores.");
    }, 
		success: function (data) { 
	var i;
	var found = false;
	for(i=0; i<data["region"][wvw_server_region].length; i++) {
		if(data["region"][wvw_server_region][i]["match_id"] == match_id) {
			found = true;
			break;
		}	
	 }
	
	if(found) {
		wvwMatch = data["region"][wvw_server_region][i];
		updateScoresHeader();

		refreshObjStatus();
		
	} else {
		console.log("Requested non-existent match");
		noMatchMsg();
	}
	}
	});
}

function refreshObjStatus() {

	$.ajax({
		url: get_obj_stats_url,
		type:'POST',
        cache: false,
		data: { matchid: match_id},
		dataType: "json",
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus + " Error: " + errorThrown);
        addJsonErrorMsg(2, "Problem updating objective status."); 
    }, 
		success: function(data) {
	objStatus = data;
	updateObjectives();
	setTimeout(refreshMatchStatus, MARKER_REFRESH_RATE);
	}
	});
}


function getMatchStatus() {
	$.ajax({
		url: get_match_stats_url,
		dataType: "json",
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus + " Error: " + errorThrown); 
        addJsonErrorMsg(3, "Problem retrieving match scores.");
    }, 
		success: function (data) { 
	var i;
	var found = false;
	for(i=0; i<data["region"][wvw_server_region].length; i++) {
		if(data["region"][wvw_server_region][i]["match_id"] == match_id) {
			found = true;
			break;
		}	
	 }
	
	if(found) {
		wvwMatch = data["region"][wvw_server_region][i];

		/* GW2 does not provide objective coordinates; pull static JSON instead */
		$.getJSON(getBaseUrl()+"json/objectives.json", function (data) { 
			wvwObjectives = data; 
			oneTimeSetup(map);
		});
		
		
	} else {
		console.log("Requested non-existent match");
		noMatchMsg();
	}
	}
	});
}

function getBaseUrl() {
    return location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/";
}

function oneTimeSetup(map) {

	// Set army names 
	var i;
	for(i=0; i<wvwMatch["worlds"].length; i++) {
		document.getElementById(wvwMatch["worlds"][i]["color"] + "_name").innerHTML = wvwMatch["worlds"][i]["name"];
	}
		
	updateScoresHeader();
	
	$.ajax({
		url: get_obj_stats_url,
		type:'POST',
        cache: false,
		data: { matchid: match_id},
		dataType: "json",
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
        console.log("Status: " + textStatus + " Error: " + errorThrown);
        addJsonErrorMsg(4, "Problem retrieving objective status."); 
    }, 
		success: function(data) {
	objStatus = data;
	addObjectiveMarkers(map);
	
	updateObjectives();
	setInterval(countdown, 1000);
	setTimeout(refreshMatchStatus, MARKER_REFRESH_RATE);
	}
	});			
}

function addJsonErrorMsg(id, msg) {
	var element =  document.getElementById("jsonerror"+id);
	if (typeof(element) == 'undefined' || element == null) {
		document.getElementById("json_error_msgs").innerHTML += createJsonErrorMsg(id, msg);
	}

}

function createJsonErrorMsg(id, msg) {
	var myerror = "<div id='jsonerror" + id + "' class='json_error_msg'><span class='red_background' style='width:90%;'><span class='json_error_delete' onclick=\"deleteJsonErrorMsg(" + id + ");\">X</span><span class='vs'>Error: </span>" + msg + " Please check your Internet connection and refresh the page.</span></div>";
	
	return myerror;

}

function deleteJsonErrorMsg(id) {
	var e = document.getElementById("jsonerror"+id);
	e.parentNode.removeChild(e);
}
/*

* values will need updating

Before map load:

0) http://gw2stats.net/api/matches.json 
	
	for matches by region
	
	Before map selection:
		region
			army names, match start/end date
		
			get "map_owner_name" from #2?
	
	Store:
		score*, ppt*, objective summary*, ratings*?
	
	
	"region" 
		("na", "eu", etc)
			"bloodlust" 
				"match_id"
				"{color}_owner_id" (red, green, blue)
			"start_date"
			"end_date"
			"match_id"
			"worlds"
				"world_id"
				"name"
				"color"  : "Red" || "Green" || "Blue"
				"score"
				"ppt"
				"objectives"
					"camps"
					"castles"
					"towers"
					"keeps"
				"rating"
					...

After map load: 

1) https://dl.dropboxusercontent.com/u/59173312/objectives.json 
	for objective id's, score, coords, and icon symbol types
	
	"{id}"
		"score" : int
		"coords"
		"type"
		"name"
			"en"
		
	
	
2) http://gw2stats.net/api/objectives.json?type=match?id={}
	for:
	on marker:
		 objective names, ri_remaining (timer value)*, army color*, guild existence*
	on marker popup:
		 time_held*, points, guild name* and img_url*
	other:
		capture time*, previous owner*
		FROM EARLIER: score*, ppt*, objective summary*, ratings*?
		
		
	"maps"
		"name" : "{Color} Borderlands" (Red, Green, Blue, Eternal)
		"map_id"  -- N/A for now?
		"map_owner_id" : one of the army id's
		"map_owner_name"
		"objectives"
			"{id}" : integer
			"name"
			"cardinal" : shorter name
			"points"
			"capture_time" : "2014-02-20 07:09:13 GMT"
			"ri_remaining" : "0:00" MAY NOT EXIST
			"time_held" : "0:00:00:00"
			"current_guild" MAY NOT EXIST
				"id" -- N/A for now?
				"name" sometimes : " []" but has id and img_url
				"img_url" : e.g. "http:\/\/gw2stats.net\/emblems\/{signature}.png" 
			"current_owner"
				"world_id" : army id integer || 0 for neutral
				"name" : null for neutral
				"color" : "RED" || "GREEN" || "BLUE" || null for neutral
			"previous_owner"
				...



*/

}  

