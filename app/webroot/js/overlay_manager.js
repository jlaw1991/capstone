if(typeof(L) !== 'undefined') {

var overlayControl = L.control.layers(null);
var overlays = new Array();
var numOverlays = 0;
var hasOverlayControl = false;

// Used by saveAsLayerGroup to determine if layergroup name
// was user-generated or default
var lastNewName = "";

var MAX_NAME_CHARS = 30;

// TODO: 
// -- check whether this commander has reached sketch max?
function uploadNewSketch() {
/*
	var canvas = document.getElementById(canvasID);
	
	// Saves to user's Downloads folder
	canvas.toBlob(function(blob) {
			saveAs(blob, nextName() + ".png");
	}, "image/png");
*/
var dataUrl = document.getElementById(canvasID).toDataURL();
var tl = map.containerPointToLatLng([0,0]);
var br = map.containerPointToLatLng([map.getSize().x, map.getSize().y]);
var title = nextName();

// Save new imagegroup to database
$.ajax({
        type:'POST',
        cache: false,
        data: {
        		base64: dataUrl,
        		title: title,
        		tlx: tl.lat,
        		tly: tl.lng,
        		brx: br.lat,
        		bry: br.lng
        	},
        url: upload_base_64_url
    }).done(function(response) {
  		//console.log(response);
  		console.log(response);
  		var jsonResponse = jQuery.parseJSON(response);

  		var layer = makeOverlayLayer(jsonResponse["image_id"], tl, br); 
  		
  		// TODO: some response if we receive null
  		addNewOverlay(title, jsonResponse["image_group_id"], layer, squad_set);
  		exitSketchMode();
}).fail(function(response) {
	addJsonErrorMsg(7, "Unable to save sketch.");
	exitSketchMode();
});

}

function addNewOverlay(title, image_group_id, layer, makeBold) {
	var newOverlay = new Object();
	newOverlay.layer = layer;
	newOverlay.title = title;
	
	var layerid = L.Util.stamp(layer);
	newOverlay.name = makeOverlayName(title, layerid, makeBold);
	newOverlay.image_group_id = image_group_id;

	overlays[layerid] = newOverlay;
	
	map.addLayer(layer);
	 if(!hasOverlayControl) {
    	map.addControl(overlayControl);
    	if(is_commander)
    		map.addControl(saveControl);
    	hasOverlayControl = true;
    }
	overlayControl.addOverlay(layer, newOverlay.name);
	numOverlays++;	
	console.log(overlays);
}

function makeOverlayLayer(url, tl, br) {
	var newImageBounds = [tl, br];
 	var newImageUrl = get_image_file_url + '/' + url;
 	
 	return new L.ImageOverlay(newImageUrl, newImageBounds); 
}

function makeOverlayName(n, id, makeBold) {
	
	if(n.length > MAX_NAME_CHARS) {
		n = n.substr(0, MAX_NAME_CHARS-3) + "...";
	}
	
	if(!is_commander) return "<span class='my-layer-item'>" + n + "</span>";
	
	var style = "";
	if(makeBold) {
		style = "style='font-weight: bold;'";
	}
	return "<span class='my-layer-item' " + style + ">" + n + "</span><span class='delete' onclick='deleteOverlay("+id+")'> &nbspX&nbsp </span>";

}

function localDeleteOverlay(id) {
		overlayControl.removeLayer(overlays[id].layer);
		map.removeLayer(overlays[id].layer);
  		overlays[id] = null;
		numOverlays--;
	
		if(numOverlays < 1) {
			map.removeControl(overlayControl);
			if(is_commander)
				map.removeControl(saveControl);
			hasOverlayControl = false;
		}
}

function deleteOverlay(id) {
	
	// delete from database/server
	$.ajax({
        type:'POST',
        cache: false,
        data: {
        		image_group_id: overlays[id].image_group_id
        	},
        url: delete_image_group_url
    }).done(function(response) {
    	console.log("deleteOverlay: " + response);
    	localDeleteOverlay(id);
    
  	}).fail(function(response) {
  		addJsonErrorMsg(8, "Unable to delete sketch from app server."); 
});
	
}

// Use the date as the default name because it's
// the most easily generated unique, human-readable string   
function nextName() {
	var name = new Date().toString();
	lastNewName = name;
	return name;
}

function saveAsLayerGroup() {
	console.log(map._layers);
	var toRemove = new Array();
	var newName = document.getElementById("save-as").value;
	var newLayerGroup = new L.layerGroup();
	
	for(key in overlays) {
		if (arrayHasOwnIndex(overlays, key)) {
			if(map.hasLayer(overlays[key].layer)) {
				// If this is a single imageOverlay
				if(map._layers[key]._layers == undefined) {
					// Add it to the new LayerGroup
					newLayerGroup.addLayer(map._layers[key]);
				
				// If this is a LayerGroup
				} else {
					// For each layer in this LayerGroup:
					map._layers[key].eachLayer(function (layer) {
		
    					// Add it to the new LayerGroup
						newLayerGroup.addLayer(layer);
					});
				}
				toRemove.push(key);
			}
		}
	}
	console.log("toRemove" + toRemove[0]);
	// Check if user is reusing a layer name 
	var reusing = false;
	for(var i=0; i<toRemove.length; i++) {
		if(newName == overlays[toRemove[i]].title) {
			reusing = true;
			break;
		}
	}
	
	// Make sure user's chosen name is unique
	if(reusing || newName == lastNewName) {
	// Don't need to verify uniqueness of default or reused name
		finishSaveAsLayerGroup(newLayerGroup, toRemove, newName);
	} else {
		
		$.ajax({
        	type:'POST',
        	cache: false,
        	data: {
        		name: newName
        	},
        	url: get_unique_name_url
    	}).done(function(response) {
    		console.log(response);
			finishSaveAsLayerGroup(newLayerGroup, toRemove, response);
		}).fail(function(response) {
			exitModal();
			addJsonErrorMsg(9, "Save failed."); 
		});
			
	}

}

function finishSaveAsLayerGroup(newLayerGroup, toRemove, newName) {
	var image_groups = new Array();
	for(var i=0; i<toRemove.length; i++) {
		image_groups.push(overlays[toRemove[i]].image_group_id);
	}
	console.log("toRemove0: " + toRemove[0]);
	console.log("image_groups0: " + image_groups[0]);
	// save changed imagegroup structure to database
	$.ajax({
        type:'POST',
        cache: false,
        data: {
        		title: newName,
        		ids: image_groups
        	},
        url: merge_image_groups_url
    }).done(function(response) {

		if(!isNumber(response)) {
			exitModal();
			addJsonErrorMsg(9, "Save failed.");
			return;
		}
		
		for(var i=0; i<toRemove.length; i++) {
			// Remove this layer from the layer control,
			// the overlays array, and the map
			overlayControl.removeLayer(map._layers[toRemove[i]]);
			overlays[toRemove[i]] = null;
			numOverlays--;
			map.removeLayer(map._layers[toRemove[i]]);
		}
		
		var newOverlay = new Object();
		newOverlay.layer = newLayerGroup;
		newOverlay.title = newName; 
		var layerid = L.Util.stamp(newOverlay.layer);
		newOverlay.name = makeOverlayName(newName, layerid, squad_set);
		newOverlay.image_group_id = response;

		overlays[layerid] = newOverlay;
	
		newLayerGroup.addTo(map);

		overlayControl.addOverlay(newLayerGroup, newOverlay.name);
		numOverlays++;
	
		exitModal();
	}).fail(function(response) {
			console.log(response);
			exitModal();
			addJsonErrorMsg(9, "Save failed.");
	});
	
}

// From http://stackoverflow.com/questions/1303646/check-whether-variable-is-number-or-string-in-javascript
function isNumber (o) {
  return ! isNaN (o-0) && o !== null && o.replace(/^\s\s*/, '') !== "" && o !== false;
}

}