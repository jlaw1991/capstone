CREATE table events(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    event_type_id INT NOT NULL,
    assignee_id INT NOT NULL,
    title VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
    details TEXT COLLATE utf8_unicode_ci NOT NULL,
    start DATETIME NOT NULL,
    end DATETIME NOT NULL,
    all_day TINYINT(1) NOT NULL DEFAULT '1',
    status VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Scheduled',
    active TINYINT(1) NOT NULL DEFAULT '1',
    created DATE DEFAULT NULL,
    modified DATE DEFAULT NULL,
    createdbyid INT NOT NULL,
    modifiedbyid INT NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

CREATE table users(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50),
    password VARCHAR(50),
    role VARCHAR(20),
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
);

create table image_groups(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNSIGNED NOT NULL, -- author
    title VARCHAR(255) NOT NULL,
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    created DATETIME DEFAULT NULL
);

create table images(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    filepath VARCHAR(255) NOT NULL,
    image_group_id INT UNSIGNED NOT NULL,
    tlx FLOAT NOT NULL,
    tly FLOAT NOT NULL,
    brx FLOAT NOT NULL,
    bry FLOAT NOT NULL,
    FOREIGN KEY(image_group_id) REFERENCES image_groups(id) ON DELETE CASCADE
);  

create table squads(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    match_string VARCHAR(40) NOT NULL,
    user_id INT UNSIGNED NOT NULL, -- commander
    event_id INT UNSIGNED NOT NULL,
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(event_id) REFERENCES events(id) ON DELETE CASCADE
);

create table squad_members(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNSIGNED NOT NULL, 
    squad_id INT UNSIGNED NOT NULL,
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(squad_id) REFERENCES squads(id) ON DELETE CASCADE
);
    
create table squad_sketches(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    squad_id INT UNSIGNED NOT NULL,
    image_group_id INT UNSIGNED NOT NULL,
    FOREIGN KEY(squad_id) REFERENCES squads(id) ON DELETE CASCADE,
    FOREIGN KEY(image_group_id) REFERENCES image_groups(id) ON DELETE CASCADE,
    created DATETIME DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `event_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


 

