
CREATE table users(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50),
    password VARCHAR(50),
    user_role VARCHAR(20), -- commander or squadmember
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
);


create table image_groups(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNSIGNED NOT NULL, -- author
    title VARCHAR(255) NOT NULL,
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    created DATETIME DEFAULT NULL
);

create table images(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	filepath VARCHAR(255) NOT NULL,
	image_group_id INT UNSIGNED NOT NULL,
    tlx FLOAT NOT NULL,
    tly FLOAT NOT NULL,
    brx FLOAT NOT NULL,
    bry FLOAT NOT NULL,
    FOREIGN KEY(image_group_id) REFERENCES image_groups(id) ON DELETE CASCADE
);	

create table squads(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	match_string VARCHAR(40) NOT NULL,
	user_id INT UNSIGNED NOT NULL, -- commander
    event_id INT UNSIGNED NOT NULL,
	FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(event_id) REFERENCES events(id) ON DELETE CASCADE
);

create table squad_members(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	user_id INT UNSIGNED NOT NULL, 
	squad_id INT UNSIGNED NOT NULL,
	FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
	FOREIGN KEY(squad_id) REFERENCES squads(id) ON DELETE CASCADE
);
	
create table squad_sketches(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	squad_id INT UNSIGNED NOT NULL,
	image_group_id INT UNSIGNED NOT NULL,
	FOREIGN KEY(squad_id) REFERENCES squads(id) ON DELETE CASCADE,
	FOREIGN KEY(image_group_id) REFERENCES image_groups(id) ON DELETE CASCADE,
	created DATETIME DEFAULT NULL
);



 
